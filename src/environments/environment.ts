// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  baseUrl: 'http://localhost:8080',
  delatnost_url: 'http://localhost:8080/api/delatnost',
  authenticateUrl: "authenticate",
  drzava_url: 'http://localhost:8080/api/drzava',
  valuta_url: 'http://localhost:8080/api/valuta',
  banka_url: 'http://localhost:8080/api/banka',
  klijent_url: 'http://localhost:8080/api/klijent',
  korisnik_url: 'http://localhost:8080/api/korisnik',
  racun_url: 'http://localhost:8080/api/racun',
  nalog_url: 'http://localhost:8080/api/nalog',
  ukidanjeRacuna_url: 'http://localhost:8080/api/ukidanjeRacuna'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
