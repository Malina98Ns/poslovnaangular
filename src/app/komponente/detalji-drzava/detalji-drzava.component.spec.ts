import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetaljiDrzavaComponent } from './detalji-drzava.component';

describe('DetaljiDrzavaComponent', () => {
  let component: DetaljiDrzavaComponent;
  let fixture: ComponentFixture<DetaljiDrzavaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetaljiDrzavaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetaljiDrzavaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
