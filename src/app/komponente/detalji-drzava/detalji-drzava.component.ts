import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Drzava } from 'src/app/model/drzava';
import { Valuta } from 'src/app/model/valuta';
import { DrzavaService } from 'src/app/servisi/drzava.service';
import { DodavanjeIIzmenaDrzaveComponent } from '../drzava/modal/dodavanje-i-izmena-drzave/dodavanje-i-izmena-drzave.component';

@Component({
  selector: 'app-detalji-drzava',
  templateUrl: './detalji-drzava.component.html',
  styleUrls: ['./detalji-drzava.component.css']
})
export class DetaljiDrzavaComponent implements OnInit {

  displayedColumns: string[] = ['id', 'naziv', 'sifra'];
  dataSource:any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  valute:Valuta[];
  drzava:Drzava;
  private routeSub: Subscription;

  constructor(
    private route: ActivatedRoute,
    private drzavaService: DrzavaService, 
    private dialog: MatDialog, 
    private _router: Router
  ) { 

  }

  ngOnInit(): void {
    if (localStorage.getItem('role') !== 'ROLE_ADMIN') {
      this._router.navigate(['profil']);
    }
    this.routeSub = this.route.params.subscribe(params => {
      this.drzavaService.getDrzava(params['id']).subscribe(res => {
        this.drzava=res;
        this.dataSource =new MatTableDataSource<Valuta>(res.valute);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
        error => {console.log("Neuspesno");})
    });
  }

  openDialogDrzava(){
    let dialogRef = this.dialog.open(DodavanjeIIzmenaDrzaveComponent,{data: {
      drzava: this.drzava
    }})
    dialogRef.afterClosed().subscribe((result) => {
      if(result=="cancel"){
          this.ngOnInit()
      }else if(result=="save"){
        this.drzavaService.updateDrzava(this.drzava).subscribe(
          res => {
            console.log("Uspesno.");
          },
          error => {
            console.log("Neuspesno.");
          });
      }else{
        this.ngOnInit()
      }
    });
  }
  
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
