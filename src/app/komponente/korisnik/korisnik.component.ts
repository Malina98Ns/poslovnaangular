import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Banka } from 'src/app/model/banka';
import { Korisnik } from 'src/app/model/korisnik';
import { BankaService } from 'src/app/servisi/banka.service';
import { KorisnikService } from 'src/app/servisi/korisnik.service';
import { BrisanjeKorisnikaComponent } from './modal/brisanje-korisnika/brisanje-korisnika.component';
import { DodavanjeIIzmenaKorisnikaComponent } from './modal/dodavanje-i-izmena-korisnika/dodavanje-i-izmena-korisnika.component';

@Component({
  selector: 'app-korisnik',
  templateUrl: './korisnik.component.html',
  styleUrls: ['./korisnik.component.css']
})
export class KorisnikComponent implements OnInit {

  public korisnici: Korisnik[];
  odobreniKorisnici: any = [];
  displayedColumns: string[] = ['id', 'korisnickoIme','uloga',"banka","operacije"];
  dataSource:any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private korisnikService: KorisnikService, private dialog: MatDialog,
    private _router: Router
    ) { }

  ngOnInit(): void {
    if (localStorage.getItem('role') !== 'ROLE_ADMIN') {
      this._router.navigate(['profil']);
    }
    this.odobreniKorisnici = [];
    this.korisnikService.getAllKorisnik().subscribe(res => {
      for (let i = 0; i < res.length; i++) {
        // if (res[i].banka != null || res[i].uloga == "ROLE_ADMIN") {
          if (res[i].banka == null) {
            res[i].banka = new Banka;
          }
          this.odobreniKorisnici.push(res[i]);
        // } 
      }
      this.dataSource =new MatTableDataSource<Korisnik>(this.odobreniKorisnici);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
      error => {console.log("Neuspesno");})
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  openDialogBanka(korisnik:Korisnik,operacija:String){
    if(operacija=="edit"){
    let dialogRef = this.dialog.open(DodavanjeIIzmenaKorisnikaComponent,{data: {
      korisnik: korisnik
    }})

    
    dialogRef.afterClosed().subscribe((result) => {
      if(result=="cancel"){
          this.ngOnInit()
      }else if(result=="save"){
        this.korisnikService.update(korisnik).subscribe(
          res => {
            console.log("Uspesno.");
          },
          error => {
            console.log("Neuspesno.");
          });
      }else{
        this.ngOnInit()
      }
    });

  }
  // else if(operacija=="add"){
  //   korisnik = new Korisnik;
  //   let dialogRef = this.dialog.open(DodavanjeIIzmenaKorisnikaComponent,{data: {
  //     korisnik: korisnik
  //   }})
  //   console.log(korisnik)
  //   dialogRef.afterClosed().subscribe((result) => {
  //   if(result=="save"){
  //     this.korisnikService.insertBanka(korisnik).subscribe(
  //       res => {
  //         console.log("Uspesno.");
  //         this.ngOnInit()
  //       },
  //       error => {
  //         console.log("Neuspesno.");
  //       });
  //   }
  // })

  // }
  }

}
