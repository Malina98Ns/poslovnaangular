import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BankaService } from 'src/app/servisi/banka.service';

@Component({
  selector: 'app-dodavanje-i-izmena-korisnika',
  templateUrl: './dodavanje-i-izmena-korisnika.component.html',
  styleUrls: ['./dodavanje-i-izmena-korisnika.component.css']
})
export class DodavanjeIIzmenaKorisnikaComponent implements OnInit {
  addEditForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public korisnik: any, private bankaService: BankaService
  ) { }

  ngOnInit(): void {
      this.addEditForm = this.formBuilder.group({
        korIme: [this.korisnik.korisnik.korisnickoIme,Validators.required],
        uloga: [this.korisnik.korisnik.uloga,Validators.required],
      });
  }
  
  ok(){
    let value = this.addEditForm.value
    this.korisnik.korisnik.korisnickoIme = value.korIme;
    this.korisnik.korisnik.uloga = value.uloga;
  }

}
