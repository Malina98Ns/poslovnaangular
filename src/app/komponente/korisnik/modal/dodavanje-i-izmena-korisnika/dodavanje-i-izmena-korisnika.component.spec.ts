import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DodavanjeIIzmenaKorisnikaComponent } from './dodavanje-i-izmena-korisnika.component';

describe('DodavanjeIIzmenaKorisnikaComponent', () => {
  let component: DodavanjeIIzmenaKorisnikaComponent;
  let fixture: ComponentFixture<DodavanjeIIzmenaKorisnikaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DodavanjeIIzmenaKorisnikaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DodavanjeIIzmenaKorisnikaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
