import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Banka } from 'src/app/model/banka';
import { BankaService } from 'src/app/servisi/banka.service';
import { MatDialog } from '@angular/material/dialog';
import { BrisanjeBankeComponent } from './modal/brisanje-banke/brisanje-banke.component';
import { DodavanjeIIzmenaBankeComponent } from './modal/dodavanje-i-izmena-banke/dodavanje-i-izmena-banke.component';

import { RacunService } from 'src/app/servisi/racun.service';

import { Router } from '@angular/router';


@Component({
  selector: 'app-banka',
  templateUrl: './banka.component.html',
  styleUrls: ['./banka.component.css']
})
export class BankaComponent implements OnInit {

  public banke: Banka[];
  displayedColumns: string[] = ['id', 'adresa', 'email', 'fax', 'naziv',
    'obracunskiRacun', 'sifra', 'swift', 'telefon', 'web', 'operacije'];
  dataSource: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  d: Banka;


  constructor(private bankaService: BankaService, private dialog: MatDialog, private racunService: RacunService, private _router: Router) {

  }

  ngOnInit(): void {
    if (localStorage.getItem('role') !== 'ROLE_ADMIN') {
      this._router.navigate(['profil']);
    }
    this.bankaService.getAllBanka().subscribe(res => {
      this.dataSource = new MatTableDataSource<Banka>(res);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
      error => { console.log("Neuspesno"); })
    this.racunService.getAllRacuni().subscribe(res => {
    },
      error => { console.log(error); })
  }

  openDialogDelete(banka: Banka) {
    let dialogRef = this.dialog.open(BrisanjeBankeComponent);

    dialogRef.afterClosed().subscribe((result) => {
      if (result == "da") {
        this.bankaService.deleteBanka(banka.id).then(
          () =>
            this.ngOnInit()
        )
      }
    });
  }

  openBanka(banka: Banka) {
    this._router.navigate(['banka/' + banka.id]);
  }

  openDialogBanka(banka: Banka, operacija: String) {
    if (operacija == "edit") {
      let dialogRef = this.dialog.open(DodavanjeIIzmenaBankeComponent, {
        data: {
          banka: banka
        }
      })


      dialogRef.afterClosed().subscribe((result) => {
        if (result == "cancel") {
          this.ngOnInit()
        } else if (result == "save") {
          this.bankaService.updateBanka(banka).subscribe(
            res => {
              console.log("Uspesno.");
            },
            error => {
              console.log("Neuspesno.");
            });
        } else {
          this.ngOnInit()
        }
      });

    } else if (operacija == "add") {
      banka = new Banka;
      let dialogRef = this.dialog.open(DodavanjeIIzmenaBankeComponent, {
        data: {
          banka: banka
        }
      })
      dialogRef.afterClosed().subscribe((result) => {
        if (result == "save") {
          this.bankaService.insertBanka(banka).subscribe(
            res => {
              console.log("Uspesno.");
              this.ngOnInit()
            },
            error => {
              console.log("Neuspesno.");
            });
        }
      })

    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
