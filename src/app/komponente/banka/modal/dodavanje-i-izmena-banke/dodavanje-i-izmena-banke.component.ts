import { Component, OnInit } from '@angular/core';
import { Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Banka } from 'src/app/model/banka';

@Component({
  selector: 'app-dodavanje-i-izmena-banke',
  templateUrl: './dodavanje-i-izmena-banke.component.html',
  styleUrls: ['./dodavanje-i-izmena-banke.component.css']
})
export class DodavanjeIIzmenaBankeComponent implements OnInit {

  addEditForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public banka: any,
  ) { }

  ngOnInit(): void {
    this.addEditForm = this.formBuilder.group({
      naziv: [this.banka.banka.naziv,Validators.required],
      sifra: [this.banka.banka.sifra,[Validators.required,Validators.pattern("^[A-Z]{3}")]],
      adresa: [this.banka.banka.adresa,Validators.required],
      email: [this.banka.banka.email,[Validators.required,Validators.email]],
      swift: [this.banka.banka.swift,Validators.required],
      fax: [this.banka.banka.fax,Validators.required],
      obracunskiRacun: [this.banka.banka.obracunskiRacun, Validators.required],
      telefon: [this.banka.banka.telefon,Validators.required],
      web: [this.banka.banka.web,Validators.required]
    });
  }
  ok(){
    let value = this.addEditForm.value
    this.banka.banka.sifra=value.sifra;
    this.banka.banka.naziv=value.naziv;
    this.banka.banka.adresa=value.adresa;
    this.banka.banka.email=value.email;
    this.banka.banka.swift=value.swift;
    this.banka.banka.fax=value.fax;
    this.banka.banka.obracunskiRacun=value.obracunskiRacun;
    this.banka.banka.telefon=value.telefon;
    this.banka.banka.web=value.web;
  }

}
