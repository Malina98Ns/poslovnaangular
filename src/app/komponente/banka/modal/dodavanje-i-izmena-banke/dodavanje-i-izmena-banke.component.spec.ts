import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DodavanjeIIzmenaBankeComponent } from './dodavanje-i-izmena-banke.component';

describe('DodavanjeIIzmenaBankeComponent', () => {
  let component: DodavanjeIIzmenaBankeComponent;
  let fixture: ComponentFixture<DodavanjeIIzmenaBankeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DodavanjeIIzmenaBankeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DodavanjeIIzmenaBankeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
