import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BrisanjeBankeComponent } from './brisanje-banke.component';

describe('BrisanjeBankeComponent', () => {
  let component: BrisanjeBankeComponent;
  let fixture: ComponentFixture<BrisanjeBankeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BrisanjeBankeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BrisanjeBankeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
