import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BankaKlijentiComponent } from './banka-klijenti.component';

describe('BankaKlijentiComponent', () => {
  let component: BankaKlijentiComponent;
  let fixture: ComponentFixture<BankaKlijentiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BankaKlijentiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BankaKlijentiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
