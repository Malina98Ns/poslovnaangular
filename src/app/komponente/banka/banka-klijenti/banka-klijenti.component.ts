import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Klijent } from 'src/app/model/klijent';
import { KorisnikService } from 'src/app/servisi/korisnik.service';
import { KlijentService } from 'src/app/servisi/klijent.service';

@Component({
  selector: 'app-banka-klijenti',
  templateUrl: './banka-klijenti.component.html',
  styleUrls: ['./banka-klijenti.component.css']
})
export class BankaKlijentiComponent implements OnInit {

  public klijenti: any;
  klijentiBanke = [];
  displayedColumns: string[] = ['id', 'ime', 'prezime', 'jmbg', 'adresa', 'telefon', 'tipklijenta', 'delatnost'];
  dataSource: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  k: Klijent;

  constructor(private klijentService: KlijentService, private _router: Router, private korisnikService: KorisnikService) { }

  ngOnInit(): void {
    if (localStorage.getItem('role') !== 'ROLE_IZVRSILAC') {
      this._router.navigate(['profil']);
    }
    this.klijentService.getAllKlijent().subscribe(res => {
      this.klijenti = res;

      this.korisnikService.getKorisnikByUserName(this.korisnikService.getLoggedInUserKorIme()).subscribe(kor => {
        for (let i = 0; i < this.klijenti.length; i++) {
          for (let n = 0; n < this.klijenti[i].racuni.length; n++) {
            if (this.klijenti[i].racuni[n].banka.id == kor.banka.id) {
              this.klijentiBanke.push(this.klijenti[i])
            }
          }
        }
        this.removeDuplicates(this.klijentiBanke);
        this.dataSource = new MatTableDataSource<Klijent>(this.klijentiBanke);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
        error => { console.log("Neuspesno"); })
    },
      error => { console.log("Neuspesno"); })
  }

  removeDuplicates(array) {
    array.splice(0, array.length, ...(new Set(array)))
  };

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
