import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Delatnost } from 'src/app/model/delatnost';
import { Klijent } from 'src/app/model/klijent';
import { DelatnostService } from 'src/app/servisi/delatnost.service';
import { DodavanjeIIzmenaDelatnostiComponent } from '../delatnost/modal/dodavanje-i-izmena-delatnosti/dodavanje-i-izmena-delatnosti.component';


@Component({
  selector: 'app-detalji-delatnost',
  templateUrl: './detalji-delatnost.component.html',
  styleUrls: ['./detalji-delatnost.component.css']
})
export class DetaljiDelatnostComponent implements OnInit {

  displayedColumns: string[] = ['id', 'ime', 'prezime','tipklijenta','adresa'];
  dataSource:any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  klijenti:Klijent[];
  delatnost:Delatnost;
  private routeSub: Subscription;

  constructor(
    private route: ActivatedRoute,
    private delatnostService: DelatnostService, 
    private dialog: MatDialog,
    private _router: Router
  ) { }

  ngOnInit(): void {
    if (localStorage.getItem('role') !== 'ROLE_ADMIN') {
      this._router.navigate(['profil']);
    }
    this.routeSub = this.route.params.subscribe(params => {
      this.delatnostService.getDelatnost(params['id']).subscribe(res => {
        this.delatnost=res;
        this.dataSource =new MatTableDataSource<Klijent>(res.klijenti);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
        error => {console.log("Neuspesno");})
    });
  }

  openDialogDelatnost(){
    let dialogRef = this.dialog.open(DodavanjeIIzmenaDelatnostiComponent,{data: {
      delatnost: this.delatnost
    }})
    dialogRef.afterClosed().subscribe((result) => {
      if(result=="cancel"){
          this.ngOnInit()
      }else if(result=="save"){
        this.delatnostService.updateDelatnost(this.delatnost).subscribe(
          res => {
            console.log("Uspesno.");
          },
          error => {
            console.log("Neuspesno.");
          });
      }else{
        this.ngOnInit()
      }
    });

  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
