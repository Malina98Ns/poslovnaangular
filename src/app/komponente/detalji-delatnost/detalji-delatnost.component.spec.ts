import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetaljiDelatnostComponent } from './detalji-delatnost.component';

describe('DetaljiDelatnostComponent', () => {
  let component: DetaljiDelatnostComponent;
  let fixture: ComponentFixture<DetaljiDelatnostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetaljiDelatnostComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetaljiDelatnostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
