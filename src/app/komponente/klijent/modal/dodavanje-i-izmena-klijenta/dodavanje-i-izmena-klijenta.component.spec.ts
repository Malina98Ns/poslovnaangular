import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DodavanjeIIzmenaKlijentaComponent } from './dodavanje-i-izmena-klijenta.component';

describe('DodavanjeIIzmenaKlijentaComponent', () => {
  let component: DodavanjeIIzmenaKlijentaComponent;
  let fixture: ComponentFixture<DodavanjeIIzmenaKlijentaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DodavanjeIIzmenaKlijentaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DodavanjeIIzmenaKlijentaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
