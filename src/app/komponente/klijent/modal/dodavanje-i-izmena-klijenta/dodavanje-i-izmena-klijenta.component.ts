import { Component, OnInit } from '@angular/core';
import { Inject } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-dodavanje-i-izmena-klijenta',
  templateUrl: './dodavanje-i-izmena-klijenta.component.html',
  styleUrls: ['./dodavanje-i-izmena-klijenta.component.css']
})
export class DodavanjeIIzmenaKlijentaComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public klijent: any
  ) { }

  ngOnInit(): void {
  }

}
