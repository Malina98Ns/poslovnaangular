import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BrisanjeKlijentaComponent } from './brisanje-klijenta.component';

describe('BrisanjeKlijentaComponent', () => {
  let component: BrisanjeKlijentaComponent;
  let fixture: ComponentFixture<BrisanjeKlijentaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BrisanjeKlijentaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BrisanjeKlijentaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
