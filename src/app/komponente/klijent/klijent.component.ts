import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Klijent } from 'src/app/model/klijent';
import { KlijentService } from 'src/app/servisi/klijent.service';
import { MatDialog } from '@angular/material/dialog';
import { BrisanjeKlijentaComponent } from './modal/brisanje-klijenta/brisanje-klijenta.component';
import { DodavanjeIIzmenaKlijentaComponent } from './modal/dodavanje-i-izmena-klijenta/dodavanje-i-izmena-klijenta.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-klijent',
  templateUrl: './klijent.component.html',
  styleUrls: ['./klijent.component.css']
})
export class KlijentComponent implements OnInit {

  public klijenti: Klijent[];
  displayedColumns: string[] = ['id', 'adresa', 'ime', 'prezime', 'jmbg',
    'telefon', 'odobren', 'tipKlijenta', 'korisnik', 'delatnost', 'operacije'];
  dataSource: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  d: Klijent;

  constructor(private klijentService: KlijentService, private dialog: MatDialog,
    private _router: Router
  ) { }

  ngOnInit(): void {
    if (localStorage.getItem('role') !== 'ROLE_ADMIN') {
      this._router.navigate(['profil']);
    }
    this.klijentService.getAllKlijent().subscribe(res => {
      this.dataSource = new MatTableDataSource<Klijent>(res);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
      error => { console.log("Neuspesno"); })


  }

  openDialogDelete(klijent: Klijent) {
    let dialogRef = this.dialog.open(BrisanjeKlijentaComponent);

    dialogRef.afterClosed().subscribe((result) => {
      if (result == "da") {
        this.klijentService.deleteKlijent(klijent.id).then(
          () =>
            this.ngOnInit()
        )
      }
    });
  }

  openDialogKlijent(klijent: Klijent, operacija: String) {
    if (operacija == "edit") {
      let dialogRef = this.dialog.open(DodavanjeIIzmenaKlijentaComponent, {
        data: {
          klijent: klijent
        }
      })


      dialogRef.afterClosed().subscribe((result) => {
        if (result == "cancel") {
          this.ngOnInit()
        } else if (result == "save") {
          this.klijentService.updateKlijent(klijent).subscribe(
            res => {
              console.log("Uspesno.");
            },
            error => {
              console.log("Neuspesno.");
            });
        } else {
          this.ngOnInit()
        }
      });

    } else if (operacija == "add") {
      // klijent = new Klijent;
      let dialogRef = this.dialog.open(DodavanjeIIzmenaKlijentaComponent, {
        data: {
          klijent: klijent
        }
      })

      dialogRef.afterClosed().subscribe((result) => {
        if (result == "save") {
          this.klijentService.insertKlijent(klijent).subscribe(
            res => {
              console.log("Uspesno.");
              this.ngOnInit()
            },
            error => {
              console.log("Neuspesno.");
            });
        } else if (result == "cancel") {
        }
      })

    }
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


}
