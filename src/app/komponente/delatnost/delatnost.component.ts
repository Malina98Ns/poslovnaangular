import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Delatnost } from 'src/app/model/delatnost';
import { DelatnostService } from 'src/app/servisi/delatnost.service';
import {MatDialog} from '@angular/material/dialog';
import { BrisanjeDelatnostiComponent } from './modal/brisanje-delatnosti/brisanje-delatnosti.component';
import { DodavanjeIIzmenaDelatnostiComponent } from './modal/dodavanje-i-izmena-delatnosti/dodavanje-i-izmena-delatnosti.component';
import { DetaljiDelatnostComponent } from '../detalji-delatnost/detalji-delatnost.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-delatnost',
  templateUrl: './delatnost.component.html',
  styleUrls: ['./delatnost.component.css']
})
export class DelatnostComponent implements OnInit {  

  displayedColumns: string[] = ['id', 'naziv', 'sifra','operacije'];
  dataSource:any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor( private delatnostService: DelatnostService, private dialog: MatDialog,private _router: Router) {
   }

  ngOnInit(): void {
    if (localStorage.getItem('role') !== 'ROLE_ADMIN') {
      this._router.navigate(['profil']);
    }
    this.delatnostService.getAllDelatnost().subscribe(res => {
      this.dataSource =new MatTableDataSource<Delatnost>(res);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
      error => {console.log("Neuspesno");})
  }


  openDialogDelete(delatnost:Delatnost){
    console.log(delatnost);
    let dialogRef = this.dialog.open(BrisanjeDelatnostiComponent);

    dialogRef.afterClosed().subscribe((result) => {
      if(result=="da"){
        this.delatnostService.deleteDelatnost(delatnost.id).then(
          () => 		   
          this.ngOnInit()
        )
      }
    });
  }

  
  openDialogDetalj(delatnost:Delatnost){
    console.log(delatnost);
    this._router.navigate(['delatnost/'+delatnost.id]);
  }

  openDialogDelatnost(delatnost:Delatnost,operacija:String){
    delatnost= new Delatnost;
    let dialogRef = this.dialog.open(DodavanjeIIzmenaDelatnostiComponent,{data: {
      delatnost: delatnost
    }})

    dialogRef.afterClosed().subscribe((result) => {
    if(result=="save"){
      this.delatnostService.insertDelatnost(delatnost).subscribe(
        res => {
          console.log("Uspesno.");
          this.ngOnInit()
        },
        error => {
          console.log("Neuspesno.");
        });
    }
  })

  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}