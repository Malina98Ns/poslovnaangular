import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BrisanjeDelatnostiComponent } from './brisanje-delatnosti.component';

describe('BrisanjeDelatnostiComponent', () => {
  let component: BrisanjeDelatnostiComponent;
  let fixture: ComponentFixture<BrisanjeDelatnostiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BrisanjeDelatnostiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BrisanjeDelatnostiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
