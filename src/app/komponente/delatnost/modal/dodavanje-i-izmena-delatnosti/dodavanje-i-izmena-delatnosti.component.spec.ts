import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DodavanjeIIzmenaDelatnostiComponent } from './dodavanje-i-izmena-delatnosti.component';

describe('DodavanjeIIzmenaDelatnostiComponent', () => {
  let component: DodavanjeIIzmenaDelatnostiComponent;
  let fixture: ComponentFixture<DodavanjeIIzmenaDelatnostiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DodavanjeIIzmenaDelatnostiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DodavanjeIIzmenaDelatnostiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
