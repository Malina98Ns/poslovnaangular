import { Component, OnInit } from '@angular/core';
import { Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Delatnost } from 'src/app/model/delatnost';

@Component({
  selector: 'app-dodavanje-i-izmena-delatnosti',
  templateUrl: './dodavanje-i-izmena-delatnosti.component.html',
  styleUrls: ['./dodavanje-i-izmena-delatnosti.component.css']
})
export class DodavanjeIIzmenaDelatnostiComponent implements OnInit {

  addEditForm: FormGroup;
  sifra: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public delatnost: any,
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.sifra=this.delatnost.delatnost.sifra;

    this.addEditForm = this.formBuilder.group({
      naziv: [this.delatnost.delatnost.naziv,Validators.required],
      sifra: [{value: '', disabled:true},[Validators.required,Validators.pattern("^[A-Z]{3}")]],
    });
    this.addEditForm.controls['sifra'].setValue(this.delatnost.delatnost.sifra);
    if(this.delatnost.delatnost.sifra==null){
      this.addEditForm.controls['sifra'].enable();
    }
  }

  ok(){
    let value = this.addEditForm.value
    if(this.sifra==null){
      this.delatnost.delatnost.sifra=value.sifra;
      }else{
        this.delatnost.delatnost.sifra=this.sifra;
      }
    this.delatnost.delatnost.naziv=value.naziv;
  }

}
