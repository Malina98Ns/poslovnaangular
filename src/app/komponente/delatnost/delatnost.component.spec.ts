import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DelatnostComponent } from './delatnost.component';

describe('DelatnostComponent', () => {
  let component: DelatnostComponent;
  let fixture: ComponentFixture<DelatnostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DelatnostComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DelatnostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
