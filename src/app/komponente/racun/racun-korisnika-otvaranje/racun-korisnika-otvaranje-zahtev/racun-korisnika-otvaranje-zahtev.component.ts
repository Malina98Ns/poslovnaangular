import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Klijent } from 'src/app/model/klijent';
import { BankaService } from 'src/app/servisi/banka.service';
import { DrzavaService } from 'src/app/servisi/drzava.service';
import { KlijentService } from 'src/app/servisi/klijent.service';
import { KorisnikService } from 'src/app/servisi/korisnik.service';
import { RacunService } from 'src/app/servisi/racun.service';
import { ValutaService } from 'src/app/servisi/valuta.service';

@Component({
  selector: 'app-racun-korisnika-otvaranje-zahtev',
  templateUrl: './racun-korisnika-otvaranje-zahtev.component.html',
  styleUrls: ['./racun-korisnika-otvaranje-zahtev.component.css']
})
export class RacunKorisnikaOtvaranjeZahtevComponent implements OnInit {
  valute:any;
  banka:any;
  klijent:any;

  zahtev : any = {
    klijent: '',
    bankaId: '',
    valuteId: ''
  }

  constructor(private httpRacun: RacunService, private route:ActivatedRoute ,private router: Router, private httpKlijent: KlijentService, private korisnikService: KorisnikService, private httpBanka: BankaService, private httpValuta: ValutaService, private httpDrzava: DrzavaService) { }

  ngOnInit(): void {
    this.httpValuta.getAllValuta().subscribe((res) => {this.valute = res;});
    this.httpBanka.getAllBanka().subscribe((res) => {this.banka = res;});
    this.httpKlijent.getByKorIme(this.korisnikService.getLoggedInUserKorIme()).subscribe((res) => {this.klijent = res;});
  }

  submitZahtev(param){

    let klijent = new Klijent(this.klijent.id,null,null,null,null,null,null,null,null,null,null);

    this.zahtev.klijent = klijent;
    this.httpKlijent.zahtevZaOtvaranje(this.zahtev.klijent,this.zahtev.bankaId,this.zahtev.valuteId,param).subscribe(
      data=>{

        alert("Uspesno poslat zahtev!");
      },error=>{
        alert("Greska!");
      });
          
  }

}
