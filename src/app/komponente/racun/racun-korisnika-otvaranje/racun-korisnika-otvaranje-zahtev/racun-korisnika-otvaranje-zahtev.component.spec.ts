import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RacunKorisnikaOtvaranjeZahtevComponent } from './racun-korisnika-otvaranje-zahtev.component';

describe('RacunKorisnikaOtvaranjeZahtevComponent', () => {
  let component: RacunKorisnikaOtvaranjeZahtevComponent;
  let fixture: ComponentFixture<RacunKorisnikaOtvaranjeZahtevComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RacunKorisnikaOtvaranjeZahtevComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RacunKorisnikaOtvaranjeZahtevComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
