import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Klijent } from 'src/app/model/klijent';
import { Racun } from 'src/app/model/racun';
import { BankaService } from 'src/app/servisi/banka.service';
import { DrzavaService } from 'src/app/servisi/drzava.service';
import { KlijentService } from 'src/app/servisi/klijent.service';
import { KorisnikService } from 'src/app/servisi/korisnik.service';
import { RacunService } from 'src/app/servisi/racun.service';
import { ValutaService } from 'src/app/servisi/valuta.service';

@Component({
  selector: 'app-racun-korisnika-otvaranje',
  templateUrl: './racun-korisnika-otvaranje.component.html',
  styleUrls: ['./racun-korisnika-otvaranje.component.css']
})
export class RacunKorisnikaOtvaranjeComponent implements OnInit {

  public racuni: any;
  racunZaAktivacijuKorisnika: any = [];
  valute:any;
  drzave:any;
  klijent:any;
  displayedColumns: string[] = ['id', 'brojRacuna', 'datumKreiranja', 'valuta','banka','operacije'];
  dataSource : any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private httpRacun: RacunService, private route:ActivatedRoute, private router: Router, private httpKlijent: KlijentService, private korisnikService: KorisnikService, private httpBanka: BankaService, private httpValuta: ValutaService, private httpDrzava: DrzavaService) { }


  ngOnInit(): void {
    if (localStorage.getItem('role') !== 'ROLE_KORISNIK') {
      this.router.navigate(['profil']);
    }
    this.httpRacun.aktivacijaRacuna(this.korisnikService.getLoggedInUserKorIme()).subscribe((res) => {
      this.racunZaAktivacijuKorisnika = res;
      this.dataSource =new MatTableDataSource<Racun>(this.racunZaAktivacijuKorisnika);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
    this.httpValuta.getAllValuta().subscribe((res) => {this.valute = res;});
    this.httpDrzava.getAllDrzava().subscribe((res) => {this.drzave = res;});
    this.httpKlijent.getByKorIme(this.korisnikService.getLoggedInUserKorIme()).subscribe((res) => {this.klijent = res;});
  }

  zahtevZaOtvaranje(){
    this.router.navigateByUrl('/racun-korisnika-otvaranje-zahtev')
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
