import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RacunKorisnikaOtvaranjeComponent } from './racun-korisnika-otvaranje.component';

describe('RacunKorisnikaOtvaranjeComponent', () => {
  let component: RacunKorisnikaOtvaranjeComponent;
  let fixture: ComponentFixture<RacunKorisnikaOtvaranjeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RacunKorisnikaOtvaranjeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RacunKorisnikaOtvaranjeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
