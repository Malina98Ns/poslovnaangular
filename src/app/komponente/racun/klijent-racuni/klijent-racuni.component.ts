import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Racun } from 'src/app/model/racun';
import { KorisnikService } from 'src/app/servisi/korisnik.service';
import { RacunService } from 'src/app/servisi/racun.service';

@Component({
  selector: 'app-klijent-racuni',
  templateUrl: './klijent-racuni.component.html',
  styleUrls: ['./klijent-racuni.component.css']
})
export class KlijentRacuniComponent implements OnInit {

  public racuni: any;
  racuniKlijenta: any = [];
  displayedColumns: string[] = ['id', 'brojRacuna', 'stanje', 'datumKreiranja', 'odobren'];
  dataSource: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  r: Racun;

  constructor(private racunService: RacunService, private _router: Router, private korisnikService: KorisnikService) { }

  ngOnInit(): void {
    if (localStorage.getItem('role') !== 'ROLE_IZVRSILAC') {
      this._router.navigate(['profil']);
    }
    this.racunService.getAllRacuni().subscribe(res => {
      console.log(res)
      this.racuni = res;

      this.korisnikService.getKorisnikByUserName(this.korisnikService.getLoggedInUserKorIme()).subscribe(kor => {
        for (let i = 0; i < this.racuni.length; i++) {
          if (this.racuni[i].banka.id == kor.banka.id) {
            this.racuniKlijenta.push(this.racuni[i])
          }
        }
        this.dataSource = new MatTableDataSource<Racun>(this.racuniKlijenta);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
        error => { console.log("Neuspesno"); })
    },
      error => { console.log("Neuspesno"); })
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
