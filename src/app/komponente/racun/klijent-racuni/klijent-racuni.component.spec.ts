import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KlijentRacuniComponent } from './klijent-racuni.component';

describe('KlijentRacuniComponent', () => {
  let component: KlijentRacuniComponent;
  let fixture: ComponentFixture<KlijentRacuniComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KlijentRacuniComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KlijentRacuniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
