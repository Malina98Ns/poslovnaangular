import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { UkidanjeRacuna } from 'src/app/model/ukidanjeracuna';
import { ActivatedRoute, Router } from '@angular/router';
import { KorisnikService } from 'src/app/servisi/korisnik.service';
import { UkidanjeRacunaService } from 'src/app/servisi/ukidanjeRacuna.service';

@Component({
  selector: 'app-ukidanje-racuna-izvrsilac',
  templateUrl: './ukidanje-racuna-izvrsilac.component.html',
  styleUrls: ['./ukidanje-racuna-izvrsilac.component.css']
})
export class UkidanjeRacunaIzvrsilacComponent implements OnInit {

  public racuniZaUkidanje: any;
  racuniKlijentaZaUkidanje: any = [];
  displayedColumns: string[] = ['id', 'klijent', 'racunZaUkidanje', 'racunZaPrenosNovca', 'stanjeRacuna', 'obrazlozenje', 'operacije'];
  dataSource : any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  u:UkidanjeRacuna;

  constructor( private ukidanjeRacunaService: UkidanjeRacunaService,private _router: Router,private korisnikService: KorisnikService) { }

  ngOnInit(): void {
    if (localStorage.getItem('role') !== 'ROLE_IZVRSILAC') {
      this._router.navigate(['profil']);
    }
    this.racuniKlijentaZaUkidanje = [];
    this.ukidanjeRacunaService.getAllUkidanjeRacuna(this.korisnikService.getLoggedInUserKorIme(), 0).subscribe(res => {
      this.racuniZaUkidanje = res;
      for(let i = 0; i < this.racuniZaUkidanje.length ; i++){
          this.racuniKlijentaZaUkidanje.push(this.racuniZaUkidanje[i])
      }
      
      this.dataSource =new MatTableDataSource<UkidanjeRacuna>(this.racuniKlijentaZaUkidanje);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
      error => {console.log("Neuspesno");})
  }

  submit(param,id){
    
    if(param === 'accepted'){
      var ukidanje = new UkidanjeRacuna(id,null,null,null,null,null);
      this.ukidanjeRacunaService.updateUkidanjeRacuna(ukidanje).subscribe(
        data=>{
          this.ngOnInit();
          alert("Uspesno ukidanje racuna!");
        },error=>{
          alert(error.error);
        });
    }else if(param === 'rejected'){
      this.ukidanjeRacunaService.delete(id).subscribe(
        data=>{
          this.ngOnInit();
          alert("Uspesno odbijanje zahteva za ukidanje!");
        },error=>{
          alert(error.error);
        });
    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
