import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UkidanjeRacunaIzvrsilacComponent } from './ukidanje-racuna-izvrsilac.component';

describe('UkidanjeRacunaIzvrsilacComponent', () => {
  let component: UkidanjeRacunaIzvrsilacComponent;
  let fixture: ComponentFixture<UkidanjeRacunaIzvrsilacComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UkidanjeRacunaIzvrsilacComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UkidanjeRacunaIzvrsilacComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
