import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Klijent } from 'src/app/model/klijent';
import { Racun } from 'src/app/model/racun';
import { KlijentService } from 'src/app/servisi/klijent.service';
import { KorisnikService } from 'src/app/servisi/korisnik.service';
import { RacunService } from 'src/app/servisi/racun.service';

@Component({
  selector: 'app-racun',
  templateUrl: './racun.component.html',
  styleUrls: ['./racun.component.css']
})
export class RacunComponent implements OnInit {


  public racuni: any;
  racunKorisnika: any = [];
  displayedColumns: string[] = ['id', 'brojRacuna', 'stanje', 'datumKreiranja', 'trenutniIznos','banka','valuta','operacije'];
  dataSource : any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  k:Klijent;
  datumOd;
  datumDo;

  constructor(private httpRacun: RacunService, private route:ActivatedRoute, private router: Router, 
    private httpKlijent: KlijentService, private korisnikService: KorisnikService) { }

  ngOnInit(): void {
    if (localStorage.getItem('role') !== 'ROLE_KORISNIK') {
      this.router.navigate(['profil']);
    }
    this.httpRacun.getAllRacuni().subscribe((res) => {
      this.racuni = res;
      for(let i = 0; i < this.racuni.length ; i++){
        if(this.racuni[i].klijent.korisnik.korisnickoIme == this.korisnikService.getLoggedInUserKorIme() && this.racuni[i].izbrisan == false && this.racuni[i].odobren == true){
          this.racunKorisnika.push(this.racuni[i])
        }
      }
      this.dataSource =new MatTableDataSource<Racun>(this.racunKorisnika);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

  });
  }

  download(racunId,klijentId){
      if(this.datumOd === undefined || this.datumOd === null || this.datumDo === undefined || this.datumDo === null){
        alert("Morate selectovati oba datuma!");
        return;
      }
      let dateOd = new Date(this.datumOd).getTime();
      let dateDo = new Date(this.datumDo).getTime();
      if(dateOd - dateDo  > 0){
        alert("Pocetni datum mora biti pre krajnjeg datuma!")
        return;
      }
      this.korisnikService.getIzvestajRacun(dateOd,dateDo,racunId,klijentId);  
  }
    
  onDatumSelected(event: any,param) { // without type info
    if(param === 'od')
      this.datumOd = event.target.value;
    else if(param === 'do')
      this.datumDo = event.target.value; 
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
