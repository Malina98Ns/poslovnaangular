import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Racun } from 'src/app/model/racun';
import { ActivatedRoute, Router } from '@angular/router';
import { KorisnikService } from 'src/app/servisi/korisnik.service';
import { RacunService } from 'src/app/servisi/racun.service';

@Component({
  selector: 'app-otvaranje-racuna-izvrsilac',
  templateUrl: './otvaranje-racuna-izvrsilac.component.html',
  styleUrls: ['./otvaranje-racuna-izvrsilac.component.css']
})
export class OtvaranjeRacunaIzvrsilacComponent implements OnInit {

  zahteviOtvaranje: any = [];
  displayedColumns: string[] = ['id', 'brojRacuna', 'datumKreiranja', 'stanje', 'klijent', 'valuta', 'operacije'];
  dataSource: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  r: Racun;

  constructor(private racunService: RacunService, private _router: Router, private korisnikService: KorisnikService) { }

  ngOnInit(): void {
    if (localStorage.getItem('role') !== 'ROLE_IZVRSILAC') {
      this._router.navigate(['profil']);
    }
    this.zahteviOtvaranje = [];
    this.racunService.getRacunByIzvrsilac(this.korisnikService.getLoggedInUserKorIme()).subscribe(res => {
      this.zahteviOtvaranje = res;

      this.dataSource = new MatTableDataSource<Racun>(this.zahteviOtvaranje);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
      error => { console.log("Neuspesno"); })
  }

  submit(param, racunId) {
    if (param === 'accepted') {
      //accepted
      var racun = new Racun(racunId, null, null, null, null, null, null, null, null, null, null, null);
      this.racunService.updateRacun(racun).subscribe(
        data => {
          this.ngOnInit();
          alert("Uspesno odobren zahtev!");
        }, error => {
          this.ngOnInit();
          alert("Uspesno odobren zahtev!");
        });

    } else if (param === 'rejected') {
      //delete
      this.racunService.delete(racunId).subscribe(
        data => {
          this.ngOnInit();
          alert("Uspesno odbijen zahtev!");
        }, error => {
          this.ngOnInit();
          alert("Uspesno odbijen zahtev!");
        });

    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}

