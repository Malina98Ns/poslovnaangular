import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RacunKorisnikaUkidanjeZahtevComponent } from './racun-korisnika-ukidanje-zahtev.component';

describe('RacunKorisnikaUkidanjeZahtevComponent', () => {
  let component: RacunKorisnikaUkidanjeZahtevComponent;
  let fixture: ComponentFixture<RacunKorisnikaUkidanjeZahtevComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RacunKorisnikaUkidanjeZahtevComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RacunKorisnikaUkidanjeZahtevComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
