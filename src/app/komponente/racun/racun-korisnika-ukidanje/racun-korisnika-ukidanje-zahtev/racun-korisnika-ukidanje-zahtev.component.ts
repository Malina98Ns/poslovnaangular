import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BankaService } from 'src/app/servisi/banka.service';
import { DrzavaService } from 'src/app/servisi/drzava.service';
import { KlijentService } from 'src/app/servisi/klijent.service';
import { KorisnikService } from 'src/app/servisi/korisnik.service';
import { RacunService } from 'src/app/servisi/racun.service';
import { ValutaService } from 'src/app/servisi/valuta.service';
import { UkidanjeRacunaService } from 'src/app/servisi/ukidanjeRacuna.service';
import { UkidanjeRacuna } from 'src/app/model/ukidanjeracuna';

@Component({
  selector: 'app-racun-korisnika-ukidanje-zahtev',
  templateUrl: './racun-korisnika-ukidanje-zahtev.component.html',
  styleUrls: ['./racun-korisnika-ukidanje-zahtev.component.css']
})
export class RacunKorisnikaUkidanjeZahtevComponent implements OnInit {

  zahtevZaUkidanje : any = {
    racunZaUkidanje: '',
    racunZaPrenos: '',
    obrazlozenje: ''
  }
  zaUkidanjeRacun:any=[];
  zaPrenosRacun:any=[];

  odobreniAktivni:any=[];

  sviZahteviZaUkidanje:any=[];

  zahteviIdRacuna:any=[];


  constructor(private httpRacun: RacunService, private route:ActivatedRoute ,private router: Router, private httpKlijent: KlijentService, private korisnikService: KorisnikService, private httpBanka: BankaService, private httpValuta: ValutaService, private httpDrzava: DrzavaService, private ukidanjeRacunaService: UkidanjeRacunaService) { }

  ngOnInit(): void {
    this.httpKlijent.getByKorIme(this.korisnikService.getLoggedInUserKorIme()).subscribe((resRacuni) => {
      this.ukidanjeRacunaService.getAllUkidanjaRacunaUToku(this.korisnikService.getLoggedInUserKorIme()).subscribe((resZahtevi) => {
        this.odobreniAktivni = resRacuni.racuni;
        this.sviZahteviZaUkidanje = resZahtevi;     
        
        this.sviZahteviZaUkidanje.forEach(zahtev => {
          this.zahteviIdRacuna.push(zahtev.racunZaUkidanje.id);
        });

        this.odobreniAktivni.forEach(racun => {
          if (!this.zahteviIdRacuna.includes(racun.id)) {
            this.zaUkidanjeRacun.push(racun);
          }
        });  

      });
    });
  }
  select(event:any){

    this.zaPrenosRacun=[]
    var selectedRacun = this.zahtevZaUkidanje.racunZaUkidanje;

    this.zaUkidanjeRacun.forEach(racun => {
      if (racun !== selectedRacun) {
        this.zaPrenosRacun.push(racun);
      }
    });
    
  }

  submitZahtev(){
    var obrazlozenje = this.zahtevZaUkidanje.obrazlozenje;
    var racunZaUkidanje = this.zahtevZaUkidanje.racunZaUkidanje;
    var racunZaPrenos = this.zahtevZaUkidanje.racunZaPrenos;

    // console.log(obrazlozenje);
    // console.log(racunZaUkidanje);
    // console.log(racunZaPrenos);

    let ukidanjeRacuna;

    ukidanjeRacuna = new UkidanjeRacuna(null, obrazlozenje, null, null, racunZaUkidanje, racunZaPrenos);
    
    this.ukidanjeRacunaService.addUkidanjeRacuna(ukidanjeRacuna).subscribe(
      data=>{
        this.router.navigate(['/racun-korisnika-ukidanje']);
        alert("Uspesno poslat zahtev!");
      },error=>{
        alert("Greska!");
      });
          
  }

}
