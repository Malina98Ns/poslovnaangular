import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Racun } from 'src/app/model/racun';
import { BankaService } from 'src/app/servisi/banka.service';
import { DrzavaService } from 'src/app/servisi/drzava.service';
import { KlijentService } from 'src/app/servisi/klijent.service';
import { KorisnikService } from 'src/app/servisi/korisnik.service';
import { RacunService } from 'src/app/servisi/racun.service';
import { UkidanjeRacunaService } from 'src/app/servisi/ukidanjeRacuna.service';
import { ValutaService } from 'src/app/servisi/valuta.service';

@Component({
  selector: 'app-racun-korisnika-ukidanje',
  templateUrl: './racun-korisnika-ukidanje.component.html',
  styleUrls: ['./racun-korisnika-ukidanje.component.css']
})
export class RacunKorisnikaUkidanjeComponent implements OnInit {

  constructor(private httpRacun: RacunService,private httpUkidanjeRacuna: UkidanjeRacunaService, private route:ActivatedRoute, private router: Router, private httpKlijent: KlijentService, private korisnikService: KorisnikService, private httpBanka: BankaService, private httpValuta: ValutaService, private httpDrzava: DrzavaService) { }

  public racuni: any;
  racunZaUkidanjeKorisnika: any = [];
  valute:any;
  drzave:any;
  klijent:any;
  displayedColumns: string[] = ['id', 'datumKreiranja', 'racunZaUkidanje', 'racunZaPrenosNovca','stanjeRacuna', 'obrazlozenje', 'operacije'];
  dataSource : any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  ngOnInit(): void {
    if (localStorage.getItem('role') !== 'ROLE_KORISNIK') {
      this.router.navigate(['profil']);
    }
    this.httpUkidanjeRacuna.getAllUkidanjaRacunaUToku(this.korisnikService.getLoggedInUserKorIme()).subscribe((res) => {
      this.racunZaUkidanjeKorisnika = res;
      this.dataSource =new MatTableDataSource<Racun>(this.racunZaUkidanjeKorisnika);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

  }
  zahtevZaUkidanje(){
    this.router.navigateByUrl('/racun-korisnika-ukidanje-zahtev');
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
