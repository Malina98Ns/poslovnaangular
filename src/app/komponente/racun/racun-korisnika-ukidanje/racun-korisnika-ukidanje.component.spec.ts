import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RacunKorisnikaUkidanjeComponent } from './racun-korisnika-ukidanje.component';

describe('RacunKorisnikaUkidanjeComponent', () => {
  let component: RacunKorisnikaUkidanjeComponent;
  let fixture: ComponentFixture<RacunKorisnikaUkidanjeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RacunKorisnikaUkidanjeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RacunKorisnikaUkidanjeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
