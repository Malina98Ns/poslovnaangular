import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Nalog } from 'src/app/model/nalog';
import { Racun } from 'src/app/model/racun';
import { DrzavaService } from 'src/app/servisi/drzava.service';
import { KorisnikService } from 'src/app/servisi/korisnik.service';
import { NalogService } from 'src/app/servisi/nalog.service';
import { RacunService } from 'src/app/servisi/racun.service';
import { ValutaService } from 'src/app/servisi/valuta.service';

@Component({
  selector: 'app-transakcije',
  templateUrl: './transakcije.component.html',
  styleUrls: ['./transakcije.component.css']
})
export class TransakcijeComponent implements OnInit {

  public racuni: any;
  racunKorisnika: any = [];
  valute: any;
  drzave: any;
  nalog: any = {
    racunDuznika: '',
    racunPrimaoca: '',
    svrhaPlacanja: '',
    pozivNaBrojZaduzenja: '',
    modelZaduzenja: '',
    pozivNaBrojOdobrenja: '',
    modelOdobrenja: '',
    drzava: '',
    valuta: '',
    iznos: '',
    hitno: ''
  }


  constructor(private httpRacun: RacunService, private _router: Router, private korisnikService: KorisnikService, private httpValuta: ValutaService, private httpDrzava: DrzavaService, private httpNalog: NalogService) { }

  ngOnInit(): void {
    if (localStorage.getItem('role') !== 'ROLE_KORISNIK') {
      this._router.navigate(['profil']);
    }
    this.httpRacun.getAllRacuni().subscribe((res) => {
      this.racuni = res;
      for (let i = 0; i < this.racuni.length; i++) {
        if (this.racuni[i].klijent.korisnik.korisnickoIme == this.korisnikService.getLoggedInUserKorIme()) {
          this.racunKorisnika.push(this.racuni[i])
        }
      }
      this.httpValuta.getAllValuta().subscribe((res) => { this.valute = res; });
      this.httpDrzava.getAllDrzava().subscribe((res) => { this.drzave = res; });
    });

  }
  submitTransakcija() {
    let primaocRacun = new Racun(null, this.nalog.racunPrimaoca, null, null, null, null, null, null, null, null, null, null);
    var transakcija = new Nalog(null, null, this.nalog.svrhaPlacanja, null, null, null, this.nalog.modelZaduzenja, this.nalog.ozivNaBrojZaduzenja, this.nalog.hitno, this.nalog.iznos, null, null, this.nalog.racunDuznika, primaocRacun,
      null, this.nalog.drzava, null, this.nalog.valuta, this.nalog.pozivNaBrojOdobrenja, this.nalog.modelOdobrenja);

    this.httpNalog.postNalog(transakcija).subscribe(


      res => {

        alert("Uspesno poslat novac!");
      }, error => {
      });
  }

}
