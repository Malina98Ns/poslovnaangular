import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Racun } from 'src/app/model/racun';
import { Valuta } from 'src/app/model/valuta';
import { DelatnostService } from 'src/app/servisi/delatnost.service';
import { ValutaService } from 'src/app/servisi/valuta.service';
import { DodavanjeIIzmenaValuteComponent } from '../valuta/modal/dodavanje-i-izmena-valute/dodavanje-i-izmena-valute.component';

@Component({
  selector: 'app-detalji-valuta',
  templateUrl: './detalji-valuta.component.html',
  styleUrls: ['./detalji-valuta.component.css']
})
export class DetaljiValutaComponent implements OnInit {

  displayedColumns: string[] = ['id', 'brojracuna', 'stanje','banka','klijent'];
  dataSource:any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  racuni:Racun[];
  valuta:Valuta;
  private routeSub: Subscription;

  constructor(
    private route: ActivatedRoute,
    private valutaService: ValutaService, 
    private dialog: MatDialog, 
    private _router: Router
  ) { }

  ngOnInit(): void {
    if (localStorage.getItem('role') !== 'ROLE_ADMIN') {
      this._router.navigate(['profil']);
    }
    this.routeSub = this.route.params.subscribe(params => {
      this.valutaService.getValuta(params['id']).subscribe(res => {
        this.valuta=res;
        this.dataSource =new MatTableDataSource<Racun>(res.racuni);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
        error => {console.log("Neuspesno");})
    });
  }

  openDialogValuta(){
    let dialogRef = this.dialog.open(DodavanjeIIzmenaValuteComponent,{data: {
      valuta: this.valuta
    }})
    dialogRef.afterClosed().subscribe((result) => {
      if(result=="cancel"){
          this.ngOnInit()
      }else if(result=="save"){
        this.valutaService.updateValuta(this.valuta).subscribe(
          res => {
            console.log("Uspesno.");
          },
          error => {
            console.log("Neuspesno.");
          });
      }else{
        this.ngOnInit()
      }
    });

  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


}
