import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetaljiValutaComponent } from './detalji-valuta.component';

describe('DetaljiValutaComponent', () => {
  let component: DetaljiValutaComponent;
  let fixture: ComponentFixture<DetaljiValutaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetaljiValutaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetaljiValutaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
