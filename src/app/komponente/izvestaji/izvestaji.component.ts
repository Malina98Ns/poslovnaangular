import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Banka } from 'src/app/model/banka';
import { Klijent } from 'src/app/model/klijent';
import { BankaService } from 'src/app/servisi/banka.service';
import { KlijentService } from 'src/app/servisi/klijent.service';
import { KorisnikService } from 'src/app/servisi/korisnik.service';

@Component({
  selector: 'app-izvestaji',
  templateUrl: './izvestaji.component.html',
  styleUrls: ['./izvestaji.component.css']
})
export class IzvestajiComponent implements OnInit {

  banke: Banka[];
  banka: Banka;
  klijent: Klijent;


  constructor(
    private bankaService: BankaService,
    private korisnikService: KorisnikService,
    private klijentService: KlijentService,
    private router: Router
  ) { }

  ngOnInit(): void {
    if (localStorage.getItem('role') !== 'ROLE_KORISNIK') {
      this.router.navigate(['profil']);
    }
    this.bankaService.getBankeKorisnika(this.korisnikService.getLoggedInUserKorIme()).subscribe(res => {
      this.banke = res
    })
    this.klijentService.getByKorIme(this.korisnikService.getLoggedInUserKorIme()).subscribe(klijent => {
      this.klijent = klijent
    },
      error => { console.log("Neuspesno"); })

  }
  ok() {
    if (this.banka != undefined && this.banka != null) {
      this.korisnikService.getIzvestajBanka(this.banka.id, this.klijent.id);
    } else {
      alert("Izaberite banku")
    }

  }
}
