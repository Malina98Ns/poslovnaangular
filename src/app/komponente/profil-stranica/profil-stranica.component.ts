import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MainNavComponent } from 'src/app/main-nav/main-nav.component';
import { Korisnik } from 'src/app/model/korisnik';
import { KlijentService } from 'src/app/servisi/klijent.service';
import { KorisnikService } from 'src/app/servisi/korisnik.service';

@Component({
  selector: 'app-profil-stranica',
  templateUrl: './profil-stranica.component.html',
  styleUrls: ['./profil-stranica.component.css']
})
export class ProfilStranicaComponent implements OnInit {
  korisnikUserName: String;
  korisnickoIme = '';
  lozinka = '';
  reLozinka = '';
  uloga = '';
  lozinkaKorisnika = '';
  korisnik: Korisnik;
  profileForm: FormGroup;
  izmena = false;
  klijenti = false;

  constructor(private _router: Router, private korisnikService: KorisnikService, private formBuilder: FormBuilder, private klijentService: KlijentService) {
  }

  ngOnInit(): void {
    this.profileForm = this.formBuilder.group({
      username: [{ value: '', disabled: true },],
      lozinka: [{ value: '', disabled: true },],
      reLozinka: [{ value: '', disabled: false },],
      uloga: [{ value: '', disabled: true },],
      imePrezime: [{ value: '', disabled: true },],
      adresa: [{ value: '', disabled: true },],
      delatnost: [{ value: '', disabled: true },],
      jmbg: [{ value: '', disabled: true },],
      telefon: [{ value: '', disabled: true },],
      tipKlijenta: [{ value: '', disabled: true },],

    })
    if(!localStorage.getItem('role')) {
      this._router.navigate(['']);
    } else {
      this.uloga = localStorage.getItem('role');
      this.korisnikUserName = this.korisnikService.getLoggedInUserKorIme();
      this.profileForm = this.formBuilder.group({
        username: [{ value: this.korisnickoIme, disabled: true }, Validators.required],
        lozinka: [{ value: this.lozinka, disabled: true }, Validators.required],
        reLozinka: [{ value: this.reLozinka, disabled: false }, Validators.required],
        uloga: [{ value: this.uloga, disabled: true }, Validators.required],
        imePrezime: [{ value: '', disabled: true }, Validators.required],
        adresa: [{ value: '', disabled: true }, Validators.required],
        delatnost: [{ value: '', disabled: true }, Validators.required],
        jmbg: [{ value: '', disabled: true }, Validators.required],
        telefon: [{ value: '', disabled: true }, Validators.required],
        tipKlijenta: [{ value: '', disabled: true }, Validators.required],

      }, { validator: this.MustMatch('lozinka', 'reLozinka') });

      if (localStorage.getItem('role') === "ROLE_KORISNIK") {
        this.loadKorisnik();
      }
      else {
        this.loadAdmin();
      }
    }
  }
  klik() {
    this.klijenti = !this.klijenti
  }

  MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        return;
      }

      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
    }
  }

  edit() {
    this.izmena = true
    this.profileForm.controls['lozinka'].enable();
    this.profileForm.patchValue({
      lozinka: '',
      reLozinka: ''

    })
    this.profileForm.controls['reLozinka'].enable();

  }
  save() {
    var lozinka = this.profileForm.get('lozinka').value;
    var kor = new Korisnik;
    kor.id = this.korisnik.id
    kor.lozinka = lozinka
    this.korisnikService.update(kor).subscribe(
      data => {
        alert('Uspesna izmena!');
        this.izmena = false;
        this.profileForm.controls['lozinka'].disable();
      }, error => {
        alert("Greska!");
      });
    this.korisnik.lozinka = kor.lozinka;
    this.profileForm.patchValue({
      lozinka: lozinka,
    });
  }

  cancel() {
    this.izmena = false;
    this.profileForm.patchValue({
      username: this.korisnik.korisnickoIme,
      lozinka: this.lozinkaKorisnika,
      uloga: this.korisnik.uloga.toLowerCase().split('_')[1],
    });
    this.profileForm.controls['lozinka'].disable();
    this.profileForm.controls['reLozinka'].disable();
  }

  loadKorisnik() {
    this.korisnikService.getKorisnikByUserName(this.korisnikUserName).subscribe(korisnik => {
      this.klijentService.getByKorIme(this.korisnikUserName).subscribe(klijent => {
        this.korisnik = korisnik;
        this.lozinkaKorisnika = localStorage.getItem('lozinka');
        this.profileForm.patchValue({
          username: korisnik.korisnickoIme,
          lozinka: this.lozinkaKorisnika,
          uloga: korisnik.uloga.toLowerCase().split('_')[1],
          imePrezime: klijent.ime + ' ' + klijent.prezime,
          adresa: klijent.adresa,
          delatnost: klijent.delatnost.naziv,
          jmbg: klijent.jmbg,
          telefon: klijent.telefon,
          tipKlijenta: klijent.tipKlijenta
        });
      },
        error => { console.log("Neuspesno"); })
    },
      error => { console.log("Neuspesno"); })
  }

  loadAdmin() {
    this.korisnikService.getKorisnikByUserName(this.korisnikUserName).subscribe(korisnik => {
      this.korisnik = korisnik;
      this.lozinkaKorisnika = localStorage.getItem('lozinka');
      console.log(this.lozinkaKorisnika);
      this.profileForm.patchValue({
        username: korisnik.korisnickoIme,
        lozinka: this.lozinkaKorisnika,
        uloga: korisnik.uloga.toLowerCase().split('_')[1],
      });
    },
      error => { console.log("Neuspesno"); })

  }

}
