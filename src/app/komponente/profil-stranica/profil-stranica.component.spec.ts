import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilStranicaComponent } from './profil-stranica.component';

describe('ProfilStranicaComponent', () => {
  let component: ProfilStranicaComponent;
  let fixture: ComponentFixture<ProfilStranicaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfilStranicaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilStranicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
