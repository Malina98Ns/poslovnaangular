import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Racun } from 'src/app/model/racun';
import { Banka } from 'src/app/model/banka';
import { BankaService } from 'src/app/servisi/banka.service';
import { DodavanjeIIzmenaBankeComponent } from '../banka/modal/dodavanje-i-izmena-banke/dodavanje-i-izmena-banke.component';

@Component({
  selector: 'app-detalji-banka',
  templateUrl: './detalji-banka.component.html',
  styleUrls: ['./detalji-banka.component.css']
})
export class DetaljiBankaComponent implements OnInit {

  displayedColumnsRacuni: string[] = ['id', 'brojracuna', 'datumkreiranja', 'stanje', 'klijent'];
  displayedColumnsKursneListe: string[] = ['id', 'broj', 'datum', 'datumprimene'];
  dataSourceRacuni: any;
  dataSourceKursneListe: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  racuni: Racun[];
  banka: Banka;
  private routeSub: Subscription;

  constructor(
    private route: ActivatedRoute,
    private bankaService: BankaService,
    private dialog: MatDialog,
    private _router: Router
  ) { }

  ngOnInit(): void {
    if (localStorage.getItem('role') !== 'ROLE_ADMIN') {
      this._router.navigate(['profil']);
    }
    this.routeSub = this.route.params.subscribe(params => {
      this.bankaService.getBanka(params['id']).subscribe(res => {
        this.banka = res;
        this.dataSourceRacuni = new MatTableDataSource<Racun>(res.racuni);
        this.dataSourceRacuni.paginator = this.paginator;
        this.dataSourceRacuni.sort = this.sort;
        this.dataSourceKursneListe = new MatTableDataSource(res.kursneListe);
        this.dataSourceKursneListe.paginator = this.paginator;
        this.dataSourceKursneListe.sort = this.sort;
      },
        error => { console.log("Neuspesno"); })
    });
  }

  openDialogBanka() {
    let dialogRef = this.dialog.open(DodavanjeIIzmenaBankeComponent, {
      data: {
        banka: this.banka
      }
    })
    dialogRef.afterClosed().subscribe((result) => {
      if (result == "cancel") {
        this.ngOnInit()
      } else if (result == "save") {
        this.bankaService.updateBanka(this.banka).subscribe(
          res => {
            console.log("Uspesno.");
          },
          error => {
            console.log("Neuspesno.");
          });
      } else {
        this.ngOnInit()
      }
    });

  }
  applyFilter(filterValue: string) {
    this.dataSourceRacuni.filter = filterValue.trim().toLowerCase();
    this.dataSourceKursneListe.filter = filterValue.trim().toLowerCase();
  }

}
