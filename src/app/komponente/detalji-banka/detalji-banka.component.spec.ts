import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetaljiBankaComponent } from './detalji-banka.component';

describe('DetaljiBankaComponent', () => {
  let component: DetaljiBankaComponent;
  let fixture: ComponentFixture<DetaljiBankaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetaljiBankaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetaljiBankaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
