import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DodavanjeIIzmenaValuteComponent } from './dodavanje-i-izmena-valute.component';

describe('DodavanjeIIzmenaValuteComponent', () => {
  let component: DodavanjeIIzmenaValuteComponent;
  let fixture: ComponentFixture<DodavanjeIIzmenaValuteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DodavanjeIIzmenaValuteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DodavanjeIIzmenaValuteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
