import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Drzava } from 'src/app/model/drzava';
import { DrzavaService } from 'src/app/servisi/drzava.service';

@Component({
  selector: 'app-dodavanje-i-izmena-valute',
  templateUrl: './dodavanje-i-izmena-valute.component.html',
  styleUrls: ['./dodavanje-i-izmena-valute.component.css']
})
export class DodavanjeIIzmenaValuteComponent implements OnInit {

  addEditForm: FormGroup;
  listaDrzava: any;
  sifra: string;

  constructor(  @Inject(MAT_DIALOG_DATA) public valuta: any , private drzavaService: DrzavaService,private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.sifra=this.valuta.valuta.sifra;
    this.drzavaService.getAllDrzava().subscribe(res => {
      this.listaDrzava = res;
    },
      error => {console.log("Neuspesno");})

      if(this.valuta.valuta.drzava==null){
        this.valuta.valuta.drzava = new Drzava;
      }
      this.addEditForm = this.formBuilder.group({
        naziv: [this.valuta.valuta.naziv,Validators.required],
        sifra: [{value: '', disabled:true},[Validators.required,Validators.pattern("^[A-Z]{3}")]],
        drzava: ['',[Validators.required]],
      });
      this.addEditForm.controls['drzava'].setValue(this.valuta.valuta.drzava.sifra);
      this.addEditForm.controls['sifra'].setValue(this.valuta.valuta.sifra);

      if(this.valuta.valuta.sifra==null){
        this.addEditForm.controls['sifra'].enable();
      }
  }

  compareCategoryObjects(object1: any, object2: any) {
    return object1 && object2 && object1.id == object2.id;
}
  ok(){
    let value = this.addEditForm.value
    this.valuta.valuta.naziv=value.naziv;
    if(this.sifra==null){
    this.valuta.valuta.sifra=value.sifra;
    }else{
      this.valuta.valuta.sifra=this.sifra;
    }
    this.listaDrzava.forEach(drzava => {
      if(drzava.sifra===value.drzava){
        this.valuta.valuta.drzava=drzava;
      }
      
    });
  }

  
}
