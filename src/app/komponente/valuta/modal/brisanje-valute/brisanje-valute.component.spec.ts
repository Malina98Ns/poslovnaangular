import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BrisanjeValuteComponent } from './brisanje-valute.component';

describe('BrisanjeValuteComponent', () => {
  let component: BrisanjeValuteComponent;
  let fixture: ComponentFixture<BrisanjeValuteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BrisanjeValuteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BrisanjeValuteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
