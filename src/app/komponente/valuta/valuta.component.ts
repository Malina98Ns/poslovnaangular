import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Valuta } from 'src/app/model/valuta';
import { ValutaService } from 'src/app/servisi/valuta.service';
import {MatDialog} from '@angular/material/dialog';
import { BrisanjeValuteComponent } from './modal/brisanje-valute/brisanje-valute.component';
import { DodavanjeIIzmenaValuteComponent } from './modal/dodavanje-i-izmena-valute/dodavanje-i-izmena-valute.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-valuta',
  templateUrl: './valuta.component.html',
  styleUrls: ['./valuta.component.css']
})
export class ValutaComponent implements OnInit {

  public valute: Valuta[];
  displayedColumns: string[] = ['id', 'naziv', 'sifra', 'drzava','operacije'];
  dataSource:any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  d:Valuta;

  constructor( private valutaService: ValutaService, private dialog: MatDialog,private _router: Router) {
   }

   ngOnInit(): void {
    if (localStorage.getItem('role') !== 'ROLE_ADMIN') {
      this._router.navigate(['profil']);
    }
    this.valutaService.getAllValuta().subscribe(res => {
      this.dataSource =new MatTableDataSource<Valuta>(res);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
      error => {console.log("Neuspesno");})
  }

  openDialogDelete(valuta:Valuta){
    let dialogRef = this.dialog.open(BrisanjeValuteComponent);

    dialogRef.afterClosed().subscribe((result) => {
      if(result=="da"){
        this.valutaService.deleteValuta(valuta.id).then(
          () => 		   
          this.ngOnInit()
        )
      }
    });
  }

  openValuta(valuta:Valuta){
    this._router.navigate(['valuta/'+valuta.id]);
  }
  
  openDialogValuta(valuta:Valuta,operacija:String){
    valuta= new Valuta;
    let dialogRef = this.dialog.open(DodavanjeIIzmenaValuteComponent,{data: {
      valuta: valuta
    }})

    dialogRef.afterClosed().subscribe((result) => {
    if(result=="save"){
      this.valutaService.insertValuta(valuta).subscribe(
        res => {
          console.log("Uspesno.");
          this.ngOnInit()
        },
        error => {
          console.log("Neuspesno.");
        });
    }
  })

  
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
