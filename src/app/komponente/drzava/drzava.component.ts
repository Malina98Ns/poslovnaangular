import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Drzava } from 'src/app/model/drzava';
import { DrzavaService } from 'src/app/servisi/drzava.service';
import { MatDialog } from '@angular/material/dialog';
import { BrisanjeDrzaveComponent } from './modal/brisanje-drzave/brisanje-drzave.component';
import { DodavanjeIIzmenaDrzaveComponent } from './modal/dodavanje-i-izmena-drzave/dodavanje-i-izmena-drzave.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-drzava',
  templateUrl: './drzava.component.html',
  styleUrls: ['./drzava.component.css']
})
export class DrzavaComponent implements OnInit {

  displayedColumns: string[] = ['id', 'naziv', 'sifra', 'operacije'];
  dataSource: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private drzavaService: DrzavaService, private dialog: MatDialog, private _router: Router) { }

  ngOnInit(): void {
    if (localStorage.getItem('role') !== 'ROLE_ADMIN') {
      this._router.navigate(['profil']);
    }
    this.drzavaService.getAllDrzava().subscribe(res => {
      this.dataSource = new MatTableDataSource<Drzava>(res);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    },
      error => { console.log("Neuspesno"); })
  }

  openDialogDelete(drzava: Drzava) {
    let dialogRef = this.dialog.open(BrisanjeDrzaveComponent);

    dialogRef.afterClosed().subscribe((result) => {
      if (result == "da") {
        this.drzavaService.deleteDrzava(drzava.id).then(
          () =>
            this.ngOnInit()
        )
      }
    });
  }

  openDialogDrzava(drzava: Drzava, operacija: String) {
    if (operacija == "edit") {
      let dialogRef = this.dialog.open(DodavanjeIIzmenaDrzaveComponent, {
        data: {
          drzava: drzava
        }
      })


      dialogRef.afterClosed().subscribe((result) => {
        if (result == "cancel") {
          this.ngOnInit()
        } else if (result == "save") {
          this.drzavaService.updateDrzava(drzava).subscribe(
            res => {
              console.log("Uspesno.");
            },
            error => {
              console.log("Neuspesno.");
            });
        } else {
          this.ngOnInit()
        }
      });

    } else if (operacija == "add") {
      drzava = new Drzava;
      let dialogRef = this.dialog.open(DodavanjeIIzmenaDrzaveComponent, {
        data: {
          drzava: drzava
        }
      })

      dialogRef.afterClosed().subscribe((result) => {
        if (result == "save") {
          this.drzavaService.insertDrzava(drzava).subscribe(
            res => {
              console.log("Uspesno.");
              this.ngOnInit()
            },
            error => {
              console.log("Neuspesno.");
            });
        }
      })

    }
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  openDialogDetalj(drzava: Drzava) {
    this._router.navigate(['drzava/' + drzava.id]);
  }

}
