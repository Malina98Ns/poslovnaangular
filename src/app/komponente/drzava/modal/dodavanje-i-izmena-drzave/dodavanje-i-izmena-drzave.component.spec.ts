import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DodavanjeIIzmenaDrzaveComponent } from './dodavanje-i-izmena-drzave.component';

describe('DodavanjeIIzmenaDrzaveComponent', () => {
  let component: DodavanjeIIzmenaDrzaveComponent;
  let fixture: ComponentFixture<DodavanjeIIzmenaDrzaveComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DodavanjeIIzmenaDrzaveComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DodavanjeIIzmenaDrzaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
