import { Component, OnInit } from '@angular/core';
import { Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Drzava } from 'src/app/model/drzava';

@Component({
  selector: 'app-dodavanje-i-izmena-drzave',
  templateUrl: './dodavanje-i-izmena-drzave.component.html',
  styleUrls: ['./dodavanje-i-izmena-drzave.component.css']
})
export class DodavanjeIIzmenaDrzaveComponent implements OnInit {

  addEditForm: FormGroup;
  sifra: string;


  constructor(
    @Inject(MAT_DIALOG_DATA) public drzava: any,
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.sifra=this.drzava.drzava.sifra;
    this.addEditForm = this.formBuilder.group({
      naziv: [this.drzava.drzava.naziv,Validators.required],
      sifra: [{value: '', disabled:true},[Validators.required,Validators.pattern("^[A-Z]{3}")]],
    });
    this.addEditForm.controls['sifra'].setValue(this.drzava.drzava.sifra);
    if(this.drzava.drzava.sifra==null){
      this.addEditForm.controls['sifra'].enable();
    }
  }
    
  ok(){
    let value = this.addEditForm.value
    if(this.sifra==null){
      this.drzava.drzava.sifra=value.sifra;
      }
    this.drzava.drzava.naziv=value.naziv;
  }

}
