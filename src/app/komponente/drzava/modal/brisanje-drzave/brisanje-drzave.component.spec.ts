import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BrisanjeDrzaveComponent } from './brisanje-drzave.component';

describe('BrisanjeDrzaveComponent', () => {
  let component: BrisanjeDrzaveComponent;
  let fixture: ComponentFixture<BrisanjeDrzaveComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BrisanjeDrzaveComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BrisanjeDrzaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
