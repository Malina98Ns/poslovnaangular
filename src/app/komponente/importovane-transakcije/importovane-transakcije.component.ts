import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Nalog } from 'src/app/model/nalog';
import { KlijentService } from 'src/app/servisi/klijent.service';
import { KorisnikService } from 'src/app/servisi/korisnik.service';
import { NalogService } from 'src/app/servisi/nalog.service';
import { RacunService } from 'src/app/servisi/racun.service';
import { ImportComponent } from './modal/import/import.component';

@Component({
  selector: 'app-importovane-transakcije',
  templateUrl: './importovane-transakcije.component.html',
  styleUrls: ['./importovane-transakcije.component.css']
})
export class ImportovaneTransakcijeComponent implements OnInit {
 
  nalozi: Nalog[]=[];
  displayedColumns: string[] = ['id','datumprijema','racunduznika','racunprimaoca','iznos','valuta','svrhaplacanja','operacije'];
  dataSource: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  nalog:Nalog;
  files: File[] = [];

  constructor(
    private router: Router,
    private racunService: RacunService,
    private nalogService: NalogService,
    private dialog: MatDialog,
    private _router: Router
  ) { }

  ngOnInit(): void {
    if (localStorage.getItem('role') !== 'ROLE_IZVRSILAC') {
      this._router.navigate(['profil']);
    }
    this.dataSource = new MatTableDataSource<Nalog>(this.nalozi);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  openImport(){
    this.files=[]
    let dialogRef = this.dialog.open(ImportComponent,{data: {
      files:  this.files}
    });
    dialogRef.afterClosed().subscribe((result) => {
      if(result=="save"){
        var fileData: FormData  = new FormData();
        for(var i =0;i<this.files.length;i++){
          fileData.append('files', this.files[i], this.files[i].name);
        }
        this.nalogService.upload(fileData).subscribe(
          data=>{
            data.forEach( e => {
              this.nalozi.push(e);
            });
            this.ngOnInit();
          });
      }
    });

  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


}
