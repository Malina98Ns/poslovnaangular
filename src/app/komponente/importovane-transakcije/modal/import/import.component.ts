import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Nalog } from 'src/app/model/nalog';
import { NalogService } from 'src/app/servisi/nalog.service';

@Component({
  selector: 'app-import',
  templateUrl: './import.component.html',
  styleUrls: ['./import.component.css']
})
export class ImportComponent implements OnInit {

  valid=false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public files: any,
    private nalogService: NalogService

  ) {
    
  }

  ngOnInit(): void {
  }

  onSelect(event) {
    this.files.files.push(...event.addedFiles);
    this.valid=true;
  }

  onRemove(event) {
    this.files.files.splice(this.files.files.indexOf(event), 1);
    if(this.files.files.length<1){
    this.valid=false;
    }
  }
}
