import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportovaneTransakcijeComponent } from './importovane-transakcije.component';

describe('ImportovaneTransakcijeComponent', () => {
  let component: ImportovaneTransakcijeComponent;
  let fixture: ComponentFixture<ImportovaneTransakcijeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImportovaneTransakcijeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportovaneTransakcijeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
