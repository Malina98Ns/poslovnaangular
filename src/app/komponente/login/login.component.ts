import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { MainNavComponent } from 'src/app/main-nav/main-nav.component';
import { KorisnikService } from 'src/app/servisi/korisnik.service';
import { SharedService } from 'src/app/komponente/login/shared.service'


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  password:string;
  username:string;

  constructor( private _router: Router, private _korisnikService: KorisnikService,private fb: FormBuilder,private sharedService:SharedService) {
   }

  ngOnInit(): void {
  }

  loginForm = this.fb.group({
    korisnickoIme: [''],
    lozinka: ['']
    });

  submitLogin(){
    this._korisnikService.login(this.loginForm.value)
    .subscribe(
      response => {
        localStorage.setItem('jwt',response.jwt)
        localStorage.setItem('role',response.uloga)
        localStorage.setItem('lozinka', this.loginForm.value.lozinka)
        this.sharedService.sendClickEvent();
        if(response.uloga == 'ROLE_ADMIN'){
            this._router.navigate(['profil']);
        }else if(response.uloga == 'ROLE_KORISNIK'){
            this._router.navigate(['profil']);
        }else if(response.uloga == 'ROLE_IZVRSILAC'){
            this._router.navigate(['profil']);
        }
      },
      error => {
        alert('Pogresni podaci!');
      }
    );
  }

  goToOtvaranjeRacuna(){
    this._router.navigate(['zahtev-otvaranja-racuna']);
  }
}
