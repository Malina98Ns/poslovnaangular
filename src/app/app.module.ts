import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainNavComponent } from './main-nav/main-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { AppRoutingModule } from './app-routing.module';
import { DelatnostComponent } from './komponente/delatnost/delatnost.component';
import { DrzavaComponent } from './komponente/drzava/drzava.component';
import { RouterModule, Routes } from '@angular/router';
import { DelatnostService } from './servisi/delatnost.service';
import { HttpClientModule } from '@angular/common/http';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatDialogModule } from '@angular/material/dialog';
import { ValutaComponent } from './komponente/valuta/valuta.component';
import { BrisanjeDelatnostiComponent } from './komponente/delatnost/modal/brisanje-delatnosti/brisanje-delatnosti.component';
import { DodavanjeIIzmenaDelatnostiComponent } from './komponente/delatnost/modal/dodavanje-i-izmena-delatnosti/dodavanje-i-izmena-delatnosti.component';
import { BrisanjeDrzaveComponent } from './komponente/drzava/modal/brisanje-drzave/brisanje-drzave.component';
import { DodavanjeIIzmenaDrzaveComponent } from './komponente/drzava/modal/dodavanje-i-izmena-drzave/dodavanje-i-izmena-drzave.component';
import {MatInputModule} from '@angular/material/input';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrisanjeValuteComponent } from './komponente/valuta/modal/brisanje-valute/brisanje-valute.component';
import { DodavanjeIIzmenaValuteComponent } from './komponente/valuta/modal/dodavanje-i-izmena-valute/dodavanje-i-izmena-valute.component';
import { BankaComponent } from './komponente/banka/banka.component';
import { BrisanjeBankeComponent } from './komponente/banka/modal/brisanje-banke/brisanje-banke.component';
import { DodavanjeIIzmenaBankeComponent } from './komponente/banka/modal/dodavanje-i-izmena-banke/dodavanje-i-izmena-banke.component';
import { KlijentComponent } from './komponente/klijent/klijent.component';
import { BrisanjeKlijentaComponent } from './komponente/klijent/modal/brisanje-klijenta/brisanje-klijenta.component';
import { DodavanjeIIzmenaKlijentaComponent } from './komponente/klijent/modal/dodavanje-i-izmena-klijenta/dodavanje-i-izmena-klijenta.component';
import { LoginComponent } from './komponente/login/login.component';
import {MatSelectModule} from '@angular/material/select';
import { KorisnikComponent } from './komponente/korisnik/korisnik.component';
import { ProfilStranicaComponent } from './komponente/profil-stranica/profil-stranica.component';
import { RacunComponent } from './komponente/racun/racun.component';
import { OtvaranjeRacunaComponent } from './komponente/racun/otvaranje-racuna/otvaranje-racuna.component';
import { DetaljiDelatnostComponent } from './komponente/detalji-delatnost/detalji-delatnost.component';
import { DetaljiValutaComponent } from './komponente/detalji-valuta/detalji-valuta.component';
import { DetaljiDrzavaComponent } from './komponente/detalji-drzava/detalji-drzava.component';
import { BrisanjeKorisnikaComponent } from './komponente/korisnik/modal/brisanje-korisnika/brisanje-korisnika.component';
import { DodavanjeIIzmenaKorisnikaComponent } from './komponente/korisnik/modal/dodavanje-i-izmena-korisnika/dodavanje-i-izmena-korisnika.component';
import { DetaljiBankaComponent } from './komponente/detalji-banka/detalji-banka.component';
import { KlijentRacuniComponent } from './komponente/racun/klijent-racuni/klijent-racuni.component';
import { TransakcijeComponent } from './komponente/transakcije/transakcije.component';
import { RacunKorisnikaOtvaranjeComponent } from './komponente/racun/racun-korisnika-otvaranje/racun-korisnika-otvaranje.component';
import { OtvaranjeRacunaIzvrsilacComponent } from './komponente/racun/otvaranje-racuna-izvrsilac/otvaranje-racuna-izvrsilac.component';
import { RacunKorisnikaOtvaranjeZahtevComponent } from './komponente/racun/racun-korisnika-otvaranje/racun-korisnika-otvaranje-zahtev/racun-korisnika-otvaranje-zahtev.component';
import { RacunKorisnikaUkidanjeComponent } from './komponente/racun/racun-korisnika-ukidanje/racun-korisnika-ukidanje.component';
import { RacunKorisnikaUkidanjeZahtevComponent } from './komponente/racun/racun-korisnika-ukidanje/racun-korisnika-ukidanje-zahtev/racun-korisnika-ukidanje-zahtev.component';
import {MatTabsModule} from '@angular/material/tabs';
import { BankaKlijentiComponent } from './komponente/banka/banka-klijenti/banka-klijenti.component';
import { UkidanjeRacunaIzvrsilacComponent } from './komponente/racun/ukidanje-racuna-izvrsilac/ukidanje-racuna-izvrsilac.component';
import { ImportovaneTransakcijeComponent } from './komponente/importovane-transakcije/importovane-transakcije.component';
import { ImportComponent } from './komponente/importovane-transakcije/modal/import/import.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { IzvestajiComponent } from './komponente/izvestaji/izvestaji.component';

const appRoutes: Routes = [
  { path: 'delatnost', component: DelatnostComponent},
  { path: 'delatnost/:id', component: DetaljiDelatnostComponent},
  { path: 'drzava', component: DrzavaComponent},
  { path: 'drzava/:id', component: DetaljiDrzavaComponent},
  { path: 'valuta', component: ValutaComponent},
  { path: 'valuta/:id', component: DetaljiValutaComponent},
  { path: 'banka', component: BankaComponent},
  { path: 'klijent', component: KlijentComponent},
  { path: '', component: LoginComponent},
  { path: 'profil', component: ProfilStranicaComponent},
  {path: 'zahtev-otvaranja-racuna', component: OtvaranjeRacunaComponent},
  { path: 'korisnik', component: KorisnikComponent},
  { path: 'racun', component: RacunComponent},
  { path: 'racun-klijenti', component: KlijentRacuniComponent},
  { path: 'banka/:id', component: DetaljiBankaComponent},
  { path: 'transakcije', component: TransakcijeComponent},
  { path: 'racun-korisnika-otvaranje', component: RacunKorisnikaOtvaranjeComponent},
  { path: 'otvaranje-racuna-izvrsilac', component: OtvaranjeRacunaIzvrsilacComponent},
  { path: 'racun-korisnika-otvaranje-zahtev', component: RacunKorisnikaOtvaranjeZahtevComponent},
  { path: 'racun-korisnika-ukidanje', component: RacunKorisnikaUkidanjeComponent},
  { path: 'racun-korisnika-ukidanje-zahtev', component: RacunKorisnikaUkidanjeZahtevComponent},
  { path: 'importovane-transakcije', component: ImportovaneTransakcijeComponent},
  { path: 'banka-klijenti', component: BankaKlijentiComponent},
  { path: 'ukidanje-racuna-izvrsilac', component: UkidanjeRacunaIzvrsilacComponent},
  { path: 'preuzimanje-izvestaja', component: IzvestajiComponent},


]
@NgModule({
  declarations: [
    AppComponent,
    MainNavComponent,
    DelatnostComponent,
    DrzavaComponent,
    ValutaComponent,
    BrisanjeDelatnostiComponent,
    DodavanjeIIzmenaDelatnostiComponent,
    BrisanjeDrzaveComponent,
    DodavanjeIIzmenaDrzaveComponent,
    BrisanjeValuteComponent,
    DodavanjeIIzmenaValuteComponent,
    BankaComponent,
    BrisanjeBankeComponent,
    DodavanjeIIzmenaBankeComponent,
    KlijentComponent,
    BrisanjeKlijentaComponent,
    DodavanjeIIzmenaKlijentaComponent,
    LoginComponent,
    KorisnikComponent,
    ProfilStranicaComponent,
    RacunComponent,
    OtvaranjeRacunaComponent,
    DetaljiDelatnostComponent,
    DetaljiValutaComponent,
    DetaljiDrzavaComponent,
    BrisanjeKorisnikaComponent,
    DodavanjeIIzmenaKorisnikaComponent,
    DetaljiBankaComponent,
    KlijentRacuniComponent,
    TransakcijeComponent,
    RacunKorisnikaOtvaranjeComponent,
    OtvaranjeRacunaIzvrsilacComponent,
    RacunKorisnikaOtvaranjeZahtevComponent,
    RacunKorisnikaUkidanjeComponent,
    RacunKorisnikaUkidanjeZahtevComponent,
    BankaKlijentiComponent,
    UkidanjeRacunaIzvrsilacComponent,
    ImportovaneTransakcijeComponent,
    ImportComponent,
    IzvestajiComponent,

  ],
  
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatFormFieldModule,
    MatListModule,
    AppRoutingModule,
    MatSelectModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatDialogModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatTabsModule,
    NgxDropzoneModule
  ],
  providers: [DelatnostService],
  bootstrap: [AppComponent]
})
export class AppModule { }
