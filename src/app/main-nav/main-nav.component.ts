import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable, Subscription } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Router } from '@angular/router';
import { SharedService } from 'src/app/komponente/login/shared.service'
import { KorisnikService } from '../servisi/korisnik.service'

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.css']
})
export class MainNavComponent implements OnInit {
  sidenavOpened: boolean = false;
  sessionStatus: boolean;
  uloga: string;
  clickEventSubscription: Subscription;
  korisnickoIme: String = null;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver, public _router: Router, private sharedService: SharedService, private korisnikService: KorisnikService) {
    this.clickEventSubscription = this.sharedService.getClickEvent().subscribe(() => {
      this.ngOnInit();
    })
  }

  ngOnInit(): void {

    this.korisnickoIme = this.korisnikService.getLoggedInUserKorIme();

    if (localStorage.length > 0) {
      this.uloga = localStorage.getItem('role');
      if (this.uloga == null) {
        this.sessionStatus = false;
      } else {
        this.sessionStatus = true;
      }
    }

  }

  odjava() {
    localStorage.removeItem("jwt");
    localStorage.removeItem('role');
    this._router.navigate([''])
  }

  routeProfile() {
    this.uloga = localStorage.getItem('role');
    if (this.uloga == null) {
      this._router.navigateByUrl("");

    } else {
      this._router.navigateByUrl("profil");
    }
  }

  togglesideNav() {
    this.sidenavOpened = !this.sidenavOpened;
  }

}
