import { Delatnost } from "./delatnost";
import { Korisnik } from "./korisnik";
import { Racun } from './racun';

export class Klijent {
    
    public id: number;
    public adresa: string;
    public ime: string;
    public prezime: string;
    public jmbg: string;
    public telefon: string;
    public odobren: boolean;
    public tipKlijenta: string;
    public korisnik: Korisnik;
    public delatnost: Delatnost;   
    racuni: Racun[] = [];

    constructor(id:number,ime:string,prezime:string,
        jmbg:string, telefon:string, adresa:string, tipKlijenta: string, korisnik: Korisnik,
        delatnost: Delatnost, racuni: Racun[], odobren: boolean){
        this.id = id;
        this.ime = ime;
        this.prezime = prezime;
        this.jmbg = jmbg;
        this.telefon = telefon;
        this.adresa = adresa;
        this.tipKlijenta = tipKlijenta;
        this.korisnik = korisnik;
        this.delatnost = delatnost;
        this.racuni = racuni;
        this.odobren = odobren;
        }
}