import { Kursnalista } from './kursnalista';
import { Racun } from './racun';

export class Banka {
    
    public id: number;
    public sifra: string;
    public naziv: string;
    public adresa: string;
    public email: string;
    public web: string;
    public telefon: string;
    public fax: string;
    public swift: string;
    public obracunskiRacun: string;
    public racuni: Racun[];
    public kursneListe: Kursnalista[];
}