import { Banka } from "./banka";
import { Klijent } from "./klijent";

export class Korisnik {
    
    public id: number;
    public korisnickoIme: string;
    public lozinka: string;
    public uloga: string;
    public banka: Banka;
    public klijent: Klijent;
}