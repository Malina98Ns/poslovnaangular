import { Klijent } from "./klijent";

export class Delatnost {
    
    public id: number;
    public sifra: string;
    public naziv: string;
    public klijenti: Klijent[];
}