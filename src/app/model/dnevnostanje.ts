import { Racun } from './racun';
import { Nalog } from './nalog';

export class DnevnoStanje{
   

   id: number;
   brojIzvoda: string;
   datumPrometa: any;
   prethodnoStanje: number;
   prometUKorist: number;
   prometNaTeret: number;
   novoStanje: number;

   racun: Racun;
   nalozi: Nalog[] = [];

   constructor(id:number,brojIzvoda:string,datumPrometa:any,
    prethodnoStanje:number, prometUKorist:number, prometNaTeret:number, novoStanje: number, 
    racun: Racun,nalozi: Nalog[]){
    this.id = id;
    this.brojIzvoda = brojIzvoda;
    this.datumPrometa = datumPrometa;
    this.prethodnoStanje = prethodnoStanje;
    this.prometUKorist = prometUKorist;
    this.prometNaTeret = prometNaTeret;
    this.novoStanje = novoStanje;
    this.racun = racun;
    this.nalozi = nalozi;
    }
}