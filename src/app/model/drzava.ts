import { Valuta } from "./valuta";

export class Drzava {

    public id: number;
    public sifra: string;
    public naziv: string;
    public valute: Valuta[];
}