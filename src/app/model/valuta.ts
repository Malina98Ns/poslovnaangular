import { Drzava } from "./drzava";
import { Racun } from "./racun";

export class Valuta {

    public id: number;
    public sifra: string;
    public naziv: string;
    public drzava: Drzava;
    public racuni: Racun[];
}