import { Banka } from "./banka";

export class Kursnalista {

    public id: number;
    public datum: Date;
    public broj: string;
    public datumPrimene: Date;
    public banka: Banka;
}
