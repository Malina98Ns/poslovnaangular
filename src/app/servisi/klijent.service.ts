import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Klijent } from '../model/klijent';

const klijent_url = environment.klijent_url;


@Injectable({
  providedIn: 'root'
})
export class KlijentService {

  private RegenerateData = new Subject<void>();
  private klijent: Klijent;
  RegenerateData$ = this.RegenerateData.asObservable();

  constructor( private http: HttpClient,private _http: HttpClient ) { }

  public getAllKlijent() : Observable<Klijent[]> {
    return this.http.get<Klijent[]>(klijent_url);
  }

  deleteKlijent(klijentId: number): Promise<{Klijent}> {
    const url = `${klijent_url}/${klijentId}`;
    return this._http
        .delete(url)
        .toPromise()
        .catch(this.handleError);
  }

  public insertKlijent (klijent: Klijent) {
    return this.http.post(klijent_url,klijent);
  }

  updateKlijent(klijent: Klijent){
    return this.http.put(klijent_url,klijent);
  }
  
    handleError(error: any): Promise<any> {
      console.error("Error... ", error);
      return Promise.reject(error.message || error);
    }
  
  zahtevZaOtvaranje(data:Klijent,bid,vid,param){
  return this._http.post<any>(`${klijent_url}/zahtev/${+bid}/${+vid}/${+param}`,data);
  }
  public getByKorIme(username:String) : Observable<Klijent> {
    return this.http.get<Klijent>(klijent_url+'/korisnik'+'/'+username);
  }
}
