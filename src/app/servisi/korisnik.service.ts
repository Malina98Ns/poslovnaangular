import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Korisnik } from '../model/korisnik';
import { JwtHelperService } from '@auth0/angular-jwt';
import * as FileSaver from 'file-saver';

const korisnik_url = environment.korisnik_url;
const base_url = environment.baseUrl;
const authenticate_url = environment.authenticateUrl;

@Injectable({
  providedIn: 'root'
})
export class KorisnikService {
  
  private RegenerateData = new Subject<void>();
  private korisnik: Korisnik;
  RegenerateData$ = this.RegenerateData.asObservable();

  constructor(private http: HttpClient,private _http: HttpClient, private _router: Router) { }

  public getAllKorisnik() : Observable<Korisnik[]> {
    return this.http.get<Korisnik[]>(korisnik_url);
  }

  public getKorisnikByUserName(username:String) : Observable<Korisnik> {
    return this.http.get<Korisnik>(korisnik_url+'/username'+'/'+username);
  }

  public getKorisnik(id: string) : Observable<Korisnik> {
    return this.http.get<Korisnik>(korisnik_url +'/'+ id);
  }

  login(userData){
    return this._http.post<any>(`${base_url}/${authenticate_url}`, userData);
  }

  getLoggedInUserKorIme(): String{
    const helper = new JwtHelperService();
    const decodedToken = helper.decodeToken(this.getToken());
    const kor = decodedToken.sub;
    return kor;
  }

  getToken(){
    return localStorage.getItem('jwt');
  }

  update(data:Korisnik){
    return this._http.put<any>(korisnik_url,data);
  }

  getIzvestajBanka(bankaId,klijentId) : any{
    return this._http.get('http://localhost:8080/api/izvestaj/download/'+bankaId+'/'+klijentId, 
    {  responseType: 'blob' as 'json' }).subscribe((res) => {
     var file = new Blob([res as BlobPart], { type: 'application/pdf' });      
     var fileName = `izvestaj_${file.size.toString()}`;   
     FileSaver.saveAs(file, fileName);
    },error=>{
      alert("Za uneti datum ne postoji izvestaj!");
    });
   }

   getIzvestajRacun(from,to,racunId,klijentId) : any{
    return this._http.get('http://localhost:8080/api/izvestaj/download/'+from+'/'+to+'/'+racunId+'/'+klijentId, 
    {  responseType: 'blob' as 'json' }).subscribe((res) => {
     var file = new Blob([res as BlobPart], { type: 'application/pdf' });      
     var fileName = `izvestaj_${file.size.toString()}`;   
     FileSaver.saveAs(file, fileName);
    },error=>{
      alert("Za uneti datum ne postoji izvestaj!");
    });
   }
}
