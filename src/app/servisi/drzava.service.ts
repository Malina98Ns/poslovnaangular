import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Drzava } from '../model/drzava';

const drzava_url = environment.drzava_url;

@Injectable({
  providedIn: 'root'
})
export class DrzavaService {

  private RegenerateData = new Subject<void>();
  private drzava: Drzava;
  RegenerateData$ = this.RegenerateData.asObservable();

  constructor(private http: HttpClient, private _http: HttpClient) { }

  public getAllDrzava() : Observable<Drzava[]> {
    return this.http.get<Drzava[]>(drzava_url);
  }

  public getDrzava(id) : Observable<Drzava> {
    return this.http.get<Drzava>(drzava_url+'/'+id);
  }

  deleteDrzava(drzavaId: number): Promise<{Drzava}> {
    const url = `${drzava_url}/${drzavaId}`;
    return this._http
        .delete(url)
        .toPromise()
        .catch(this.handleError);
  }

  public insertDrzava (drzava: Drzava) {
    return this.http.post(drzava_url,drzava);
  }
  
  
  updateDrzava(drzava: Drzava){
    return this.http.put(drzava_url,drzava);
  }

  handleError(error: any): Promise<any> {
    console.error("Error... ", error);
    return Promise.reject(error.message || error);
  }
}
