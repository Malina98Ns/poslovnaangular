import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Valuta } from '../model/valuta';

const valuta_url = environment.valuta_url;

@Injectable({
  providedIn: 'root'
})
export class ValutaService {

  private RegenerateData = new Subject<void>();
  private valuta: Valuta;
  RegenerateData$ = this.RegenerateData.asObservable();

  constructor(private http: HttpClient,private _http: HttpClient) { }

  public getAllValuta() : Observable<Valuta[]> {
    return this.http.get<Valuta[]>(valuta_url);
  }

  public getValuta(id) : Observable<Valuta> {
    return this.http.get<Valuta>(valuta_url+'/'+id);
  }

  deleteValuta(valutaId: number): Promise<{Valuta}> {
    const url = `${valuta_url}/${valutaId}`;
    return this._http
        .delete(url)
        .toPromise()
        .catch(this.handleError);
  }

  public insertValuta (valuta: Valuta) {
    return this.http.post(valuta_url,valuta);
  }
  
  
updateValuta(valuta: Valuta){
  return this.http.put(valuta_url,valuta);
}

  handleError(error: any): Promise<any> {
    console.error("Error... ", error);
    return Promise.reject(error.message || error);
  }

}
