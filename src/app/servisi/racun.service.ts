import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Racun } from '../model/racun';

const racun_url = environment.racun_url;

@Injectable({
  providedIn: 'root'
})
export class RacunService {
  private RegenerateData = new Subject<void>();
  private racun: Racun;
  RegenerateData$ = this.RegenerateData.asObservable();

  constructor(private http: HttpClient,private _http: HttpClient, private _router: Router) { }

  public getAllRacuni() : Observable<Racun[]> {
    return this.http.get<Racun[]>(racun_url);
  }

  public getRacunByIzvrsilac(username:String) : Observable<Racun> {
    return this.http.get<Racun>(racun_url+'/izvrsilac'+'/'+username);
  }

  updateRacun(data:Racun){
    return this._http.put<any>(racun_url,data);
  }

  delete(racunId:String){
    return this._http.delete<any>(racun_url+'/'+racunId);
  }

  public aktivacijaRacuna(username:String) : Observable<Racun> {
    return this.http.get<Racun>(racun_url+'/korisnikRacunAktivacija'+'/'+username);
  }

  public getRacunByBanka(username:String) : Observable<Racun> {
    return this.http.get<Racun>(racun_url+'/racuniBanke'+'/'+username);
  }
}

