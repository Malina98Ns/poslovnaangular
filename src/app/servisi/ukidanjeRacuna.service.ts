import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UkidanjeRacuna } from '../model/ukidanjeracuna';

const ukidanjeRacuna_url = environment.ukidanjeRacuna_url;

@Injectable({
  providedIn: 'root'
})
export class UkidanjeRacunaService {
  private RegenerateData = new Subject<void>();
  private ukidanjeRacuna: UkidanjeRacuna;
  RegenerateData$ = this.RegenerateData.asObservable();

  constructor(private http: HttpClient,private _http: HttpClient, private _router: Router) { }

  public getAllUkidanjeRacuna(username:String, param:Number) : Observable<UkidanjeRacuna[]> {
    return this.http.get<UkidanjeRacuna[]>(ukidanjeRacuna_url+'/izvrsilac'+'/'+username+'/'+param);
  }

  public getAllUkidanjaRacunaUToku(username:String) : Observable<UkidanjeRacuna[]>{
      return this.http.get<UkidanjeRacuna[]>(ukidanjeRacuna_url+'/klijent'+'/'+username)
  }

  public addUkidanjeRacuna (ukidanjeRacuna: UkidanjeRacuna) {
    return this.http.post(ukidanjeRacuna_url,ukidanjeRacuna);
  }

  updateUkidanjeRacuna(data:UkidanjeRacuna){
    return this._http.put<any>(ukidanjeRacuna_url,data);
  }

  delete(ukidanjeRacunaId:String){
    return this._http.delete<any>(ukidanjeRacuna_url+'/'+ukidanjeRacunaId);
  }
}

