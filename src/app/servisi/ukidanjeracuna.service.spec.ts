import { TestBed } from '@angular/core/testing';

import { UkidanjeracunaService } from './ukidanjeracuna.service';

describe('UkidanjeracunaService', () => {
  let service: UkidanjeracunaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UkidanjeracunaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
