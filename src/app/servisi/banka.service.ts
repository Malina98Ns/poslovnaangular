import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Banka } from '../model/banka';

const banka_url = environment.banka_url;

@Injectable({
  providedIn: 'root'
})
export class BankaService {

  private RegenerateData = new Subject<void>();
  private banka: Banka;
  RegenerateData$ = this.RegenerateData.asObservable();

  constructor(private http: HttpClient,private _http: HttpClient) { }

  public getAllBanka() : Observable<Banka[]> {
    return this.http.get<Banka[]>(banka_url);
  }
  public getBankeKorisnika(korisnickoIme) : Observable<Banka[]> {
    return this.http.get<Banka[]>(banka_url+'/bankeKlijenta/'+korisnickoIme);
  }

  deleteBanka(bankaId: number): Promise<{Banka}> {
    const url = `${banka_url}/${bankaId}`;
    return this._http
        .delete(url)
        .toPromise()
        .catch(this.handleError);
  }

  public getBanka(id) : Observable<Banka> {
    return this.http.get<Banka>(banka_url+'/'+id);
  }

  public insertBanka (banka: Banka) {
    return this.http.post(banka_url,banka);
  }
  
  
updateBanka(banka: Banka){
  return this.http.put(banka_url,banka);
}

  handleError(error: any): Promise<any> {
    console.error("Error... ", error);
    return Promise.reject(error.message || error);
  }

}
