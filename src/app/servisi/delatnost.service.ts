import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Delatnost } from '../model/delatnost';

const delatnost_url = environment.delatnost_url;

@Injectable({
  providedIn: 'root'
})
export class DelatnostService {

  private RegenerateData = new Subject<void>();
  private delatnost: Delatnost;
  RegenerateData$ = this.RegenerateData.asObservable();

  constructor(private http: HttpClient,private _http: HttpClient) { }

  public getAllDelatnost() : Observable<Delatnost[]> {
    return this.http.get<Delatnost[]>(delatnost_url);
  }

  public getDelatnost(id) : Observable<Delatnost> {
    return this.http.get<Delatnost>(delatnost_url+'/'+id);
  }


  deleteDelatnost(delatnostId: number): Promise<{Delatnost}> {
    const url = `${delatnost_url}/${delatnostId}`;
    return this._http
        .delete(url)
        .toPromise()
        .catch(this.handleError);
  }

  public insertDelatnost (delatnost: Delatnost) {
    return this.http.post(delatnost_url,delatnost);
  }
  
  
updateDelatnost(delatnost: Delatnost){
  return this.http.put(delatnost_url,delatnost);
}

  handleError(error: any): Promise<any> {
    console.error("Error... ", error);
    return Promise.reject(error.message || error);
  }

}
