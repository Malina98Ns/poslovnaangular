import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Nalog } from '../model/nalog';

const nalog_url = environment.nalog_url;

@Injectable({
  providedIn: 'root'
})
export class NalogService {
  private RegenerateData = new Subject<void>();
  private nalog: Nalog;
  RegenerateData$ = this.RegenerateData.asObservable();

  constructor(private http: HttpClient, private _http: HttpClient) { }

  public postNalog(nalog: Nalog) {
    return this.http.post(nalog_url,nalog);
  }
  
  upload(data){
    return this._http.post<any>(`${nalog_url}/importFiles`, data);
  }
}
